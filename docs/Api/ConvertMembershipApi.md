# Funxtion\Integration\Sportivity\Api\Client\ConvertMembershipApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**convertMembershipPost**](ConvertMembershipApi.md#convertmembershippost) | **POST** /ConvertMembership | Convert a membership to another membership

# **convertMembershipPost**
> string convertMembershipPost($body, $xAPITOKEN)

Convert a membership to another membership

To convert a membership you need to supply the membershipId of the old membership, the MembershipDefinitionId of the membership you want to convert to and the ConvertDate.        ### Example URL  https://www.sportivity.com/sportivity-api/ConvertMembership    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Convert Membershipresource  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | ConvertDate | Date | The date the membership should be converted to the new membership. Format dd/mm/yyyy. Required. |  | OldMembershipId | Integer | The MembershipId of the old membership. Required. |  | NewMembershipDefinitionId | Integer | The MembershipDefinitionId of the new membership. Required. |  | SendMail | Boolean | Whether you want to send a confirmation mail or not. Optional. |      ## Sample message:  ```  {    \"ConvertDate\": \"29/12/2021\",    \"OldMembershipId\": 1234566,    \"NewMembershipDefinitionId\": 1234,    \"SendMail\": false  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 409 | 409001 | Wrong date format|  | 409 | 409002 | ConvertDate cannot be in the past|  | 409 | 409003 | No valid membership found|  | 409 | 409004 | No valid membershipdefinition found|  | 409 | 409005 | Unexpected error|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\ConvertMembershipApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\ConvertMembership(); // \Funxtion\Integration\Sportivity\Api\Client\Model\ConvertMembership | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->convertMembershipPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConvertMembershipApi->convertMembershipPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\ConvertMembership**](../Model/ConvertMembership.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

