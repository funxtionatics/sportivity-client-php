# Funxtion\Integration\Sportivity\Api\Client\SuspensionApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**suspensionCustomerCidGet**](SuspensionApi.md#suspensioncustomercidget) | **GET** /Suspension/Customer/{cid} | Get the financial blockage of a customer
[**suspensionPost**](SuspensionApi.md#suspensionpost) | **POST** /Suspension | Post a suspension to a customer
[**suspensionUpdatePost**](SuspensionApi.md#suspensionupdatepost) | **POST** /Suspension/update | Update a suspension

# **suspensionCustomerCidGet**
> string suspensionCustomerCidGet($cid, $xAPITOKEN)

Get the financial blockage of a customer

Get the financial blockage of a customer by Customer ID  The {cid} parameter should be replaced with the Customer ID    ### Example URL  https://www.sportivity.com/sportivity-api/Suspension/Customer/1234567    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Sample response  ```  {    \"CustomerID\": 123456,    \"Blockage\": \"true\",    \"Blockages\": [      {        \"GateBlockage\": false,        \"FinancialBlockage\": true,        \"Descripton\": \"Zomerstop\",        \"AccesBlockDateStart\": \"2020-11-16T00:00:00.000Z\",        \"AccesBlockDateEnd\": \"2020-11-20T00:00:00.000Z\",        \"BlockageId\": 15432,        \"MembershipId\": 123456      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No customer could be found based on the given ID|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\SuspensionApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cid = 56; // int | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->suspensionCustomerCidGet($cid, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SuspensionApi->suspensionCustomerCidGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**|  |
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **suspensionPost**
> string suspensionPost($body, $xAPITOKEN)

Post a suspension to a customer

Post a suspension to a customer.    To post a suspension to a customer you need a MembershipId and a BlockageDefinitionId.  The StartDate is required. Leaving the EndDate empty will result in a suspension with an open EndDate.    To add a gate blockage add Gateblockage with String value \"True\" or \"true\" in the body.    ### Example URL  https://www.sportivity.com/sportivity-api/Suspension    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ##Resource  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | MembershipID | Integer | Unique identifier of the membership.l |  | Gateblockage | String | If yes also add gate blockage, default no Optional |  | StartDate | Date |  Required. Important: the date format needs to be specified as dd/MM/yyyy, for example: 31/12/1999 |  | EndDate | Date |  Required. Important: the date format needs to be specified as dd/MM/yyyy, for example: 31/12/1999 |  | BlockageDefinitionID | Integer | Unique identifier of the blockagedefintion. |      ## Sample message:  ```  {   \"MembershipID\": \"123456\",   \"StartDate\": \"14/07/2020\",   \"EndDate\": \"15/07/2020\",   \"BlockageDefinitionID\": 1234,   \"GateBlockage\": \"true\"  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | | The provided JSON content was incorrect |  | 401 | 401001 | Unauthorized |  | 404 | 404001| No Customer found|  | 404 | | No CompanyLocation found|  | 404 | 404007 | No Membership found|  | 404 | | No BlockageDefinition found|  | 404 | | No ProductDefinition found|  | 409 | | FamilyDiscount Membership|  | 409 | | Wrong EndDate|  | 409 | | Wrong StartDate|  | 409 | | Wrong Date|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\SuspensionApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\BlockageResource(); // \Funxtion\Integration\Sportivity\Api\Client\Model\BlockageResource | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->suspensionPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SuspensionApi->suspensionPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\BlockageResource**](../Model/BlockageResource.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **suspensionUpdatePost**
> string suspensionUpdatePost($body, $xAPITOKEN)

Update a suspension

Change a suspension.    Note: A suspension that already has an end date can't be changed to an open end date.    ### Example URL  https://www.sportivity.com/sportivity-api/Suspension/update    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ##Resource  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | MembershipID | Integer | Unique identifier of the membership.l |  | Gateblockage | Boolean | If yes also add gate blockage, default no Optional |  | StartDate | Date |  Required. Important: the date format needs to be specified as dd/MM/yyyy, for example: 31/12/1999 |  | EndDate | Date |  Required. Important: the date format needs to be specified as dd/MM/yyyy, for example: 31/12/1999 |  | BlockageDefinitionID | Integer | Unique identifier of the blockagedefintion. |      ## Sample message:  ```  {   \"MembershipID\": \"123456\",   \"StartDate\": \"14/07/2020\",   \"EndDate\": \"15/07/2020\",   \"BlockageDefinitionID\": 1234,   \"BlockageId\": 1234,   \"GateBlockage\": True  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | | The provided JSON content was incorrect |  | 401 | 401001 | Unauthorized |  | 404 | 404001| No Customer found|  | 404 | | No CompanyLocation found|  | 404 | 404007 | No Membership found|  | 404 | | No BlockageDefinition found|  | 404 | | No ProductDefinition found|  | 409 | | FamilyDiscount Membership|  | 409 | | Wrong EndDate|  | 409 | | Wrong StartDate|  | 409 | | Wrong Date|  | 409 | 409017 | This suspension has already beed processed and therefore cannot be changed|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\SuspensionApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\BlockageResource2(); // \Funxtion\Integration\Sportivity\Api\Client\Model\BlockageResource2 | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->suspensionUpdatePost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SuspensionApi->suspensionUpdatePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\BlockageResource2**](../Model/BlockageResource2.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

