# Funxtion\Integration\Sportivity\Api\Client\BuckarooApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**buckarooLinkPost**](BuckarooApi.md#buckaroolinkpost) | **POST** /Buckaroo/Link | Retrieve a Buckaroo payment link

# **buckarooLinkPost**
> string buckarooLinkPost($body, $xAPITOKEN)

Retrieve a Buckaroo payment link

Supply a Buckaroo resource in the body of the HTTP POST request.    If the call is successful, the response contains a link and a transaction id.  With the link, the payment can be made.  If the payment is successful, a push message will be sent with the TransactionId to an endpoint provided to boss, this should trigger your next step in the AddMembership/AddAddon call.        ### Example URL  https://www.sportivity.com/sportivity-api/Buckaroo/Link    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Buckaroo resource  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | CustomerId | Integer | Unique identifier of the customer.  |  | Amount | Decimal | The amount that should be payed |  | Bank | Enumeration |  The bank of the customer. See below for enumeration options |  | PaymentMethod | Enumeration |  The method of payment. See below for enumeration options |  | Description | String(200) |  Optional. The description of the payment |      ### Enumeration Bank  - ABN_AMRO  - ASN_Bank  - ING  - Rabobank  - SNS_Bank  - SNS_Regio_Bank  - Triodos_Bank  - Van_Lanschot  - Knab  - Bunq  - Handelsbanken  - Revolut      ### Enumeration PaymentMethod  - iDEAL  - BanContact        ## Sample message:  ```  {    \"CustomerId\": 123456,    \"Amount\": 49.95,    \"Bank\": \"ABN_AMRO\",    \"PaymentMethod\": \"iDeal\",    \"Description\": \"Abonnement onbeperkt sporten \"  }  ```        ## Sample response  ```    {    \"Link\": \"https://checkout.buckaroo.nl/html/redirect.ashx?r=74AAAD7D678E12BAB4F53BFF980A379F9674B\",    \"TransactionId\": 52792  }    ```    ## Sample push message send to the provided adress after receiving the payment  ```    {        \"Amount\": 49.95,        \"CustomersId\": 123456,        \"TransactionId\": 52792  }    ```          ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized, please contact B.o.s.s.|  | 409 | 409001 | No valid amount|  | 409 | 409002 | No valid payment methodt|  | 409 | 409003 | No valid bank|  | 409 | 409004 | No customer found for the given id|  | 500 | | Unexpected error |

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\BuckarooApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\Buckaroo(); // \Funxtion\Integration\Sportivity\Api\Client\Model\Buckaroo | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->buckarooLinkPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuckarooApi->buckarooLinkPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\Buckaroo**](../Model/Buckaroo.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

