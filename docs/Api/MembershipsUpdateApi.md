# Funxtion\Integration\Sportivity\Api\Client\MembershipsUpdateApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**membershipsUpdateChangeToDirectDebitPost**](MembershipsUpdateApi.md#membershipsupdatechangetodirectdebitpost) | **POST** /MembershipsUpdate/ChangeToDirectDebit | 
[**membershipsUpdateChangeToInvoicePost**](MembershipsUpdateApi.md#membershipsupdatechangetoinvoicepost) | **POST** /MembershipsUpdate/ChangeToInvoice | 
[**membershipsUpdateGet**](MembershipsUpdateApi.md#membershipsupdateget) | **GET** /MembershipsUpdate | Retrieve all membership updates

# **membershipsUpdateChangeToDirectDebitPost**
> string membershipsUpdateChangeToDirectDebitPost($body, $xAPITOKEN)



## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404021 | No membership found|  | 500 | 500 | Unexpected error|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\MembershipsUpdateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\MembershipCancelResource2(); // \Funxtion\Integration\Sportivity\Api\Client\Model\MembershipCancelResource2 | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->membershipsUpdateChangeToDirectDebitPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MembershipsUpdateApi->membershipsUpdateChangeToDirectDebitPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\MembershipCancelResource2**](../Model/MembershipCancelResource2.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **membershipsUpdateChangeToInvoicePost**
> string membershipsUpdateChangeToInvoicePost($body, $xAPITOKEN)



## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404021 | No membership found|  | 500 | 500 | Unexpected error|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\MembershipsUpdateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\MembershipCancelResource2(); // \Funxtion\Integration\Sportivity\Api\Client\Model\MembershipCancelResource2 | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->membershipsUpdateChangeToInvoicePost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MembershipsUpdateApi->membershipsUpdateChangeToInvoicePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\MembershipCancelResource2**](../Model/MembershipCancelResource2.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **membershipsUpdateGet**
> string membershipsUpdateGet($xAPITOKEN)

Retrieve all membership updates

Retrieve all membership updates.    ### Example URL  https://www.sportivity.com/sportivity-api/MembershipsUpdate    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Using this call in combination with the webhook:  If you're using the webhook (please refer to the webhooks below) and have received a call of the type MembershipUpdate this is the second step of receiving the updated memberships.    ## Successful response sample  ```  {    \"GetMembershipsUpdate\": [      {        \"CustomerId\": 1234567,        \"MembershipId\": 123456,        \"CancelledAtDate\": \"28/07/2020\",        \"ContractEndDate\": \"02/08/2020\",        \"CancelledPerDate\": \"02/08/2020\",        \"TerminiationDescription\": \"Verhuizing buiten regio\"      },      {        \"CustomerId\": 1234567,        \"MembershipId\": 123456,        \"CancelledAtDate\": \"29/07/2020\",        \"ContractEndDate\": \"02/08/2020\",        \"CancelledPerDate\": \"02/08/2020\",        \"TerminiationDescription\": \"Omzetting\"      },      {        \"CustomerId\": 123456,        \"MembershipId\": 123456,        \"CancelledAtDate\": \"29/07/2020\",        \"ContractEndDate\": \"02/08/2020\",        \"CancelledPerDate\": \"02/08/2020\",        \"TerminiationDescription\": \"Omzetting\"      },      {        \"CustomerId\": 123456,        \"MembershipId\": 123456,        \"NumberOfVisitsLeft\": 999      },      {        \"CustomerId\": 123456,        \"MembershipId\": 123456,        \"StartDate\": \"19/03/2020\"      }    ]  }  ```    ## Successful empty response sample  ```  {\"GetMembershipsUpdate\": [\"NoUpdates\"]}  ```      ## Response processing  You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an “Error” object with attributes “code” and \"message\".    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | | No Updates|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\MembershipsUpdateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->membershipsUpdateGet($xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MembershipsUpdateApi->membershipsUpdateGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

