# Funxtion\Integration\Sportivity\Api\Client\LeadsApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**leadsByEmailEmailGet**](LeadsApi.md#leadsbyemailemailget) | **GET** /Leads/ByEmail/{email} | Get Lead by email
[**leadsPost**](LeadsApi.md#leadspost) | **POST** /Leads | Post a new lead

# **leadsByEmailEmailGet**
> string leadsByEmailEmailGet($email, $xAPITOKEN)

Get Lead by email

Retrieve a lead using the email.  The {email} parameter should be replaced with the email.      ### Example URL  https://www.sportivity.com/sportivity-api/Leads/ByEmail/{email}    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"      ## Successfull response sample:  ```  {    \"Fullname\": \"John Doe\",    \"Firstname\": \"John\",    \"Middlename\": \"\",    \"Lastname\": \"Doe\",    \"Email\": \"johndoe@test.com\",    \"Phone\": \"0612345678\",    \"AdditionalInfo\": \"This is additional info\",    \"CampaignName\": \"Campaingname\"  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400016 | Email is empty|  | 400 | 400026 | Invalid Email|  | 401 | 401001 | Unauthorized |  | 404 | 400026 | Lead not found with given email|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\LeadsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$email = "email_example"; // string | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->leadsByEmailEmailGet($email, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LeadsApi->leadsByEmailEmailGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **string**|  |
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **leadsPost**
> string leadsPost($body, $xAPITOKEN)

Post a new lead

Supply a Lead resource in the body of the HTTP POST request.    ### Example URL  https://www.sportivity.com/sportivity-api/Leads    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Lead Resource  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | Fullname | String(200) | required |  | Firstname | String(200) | Optional |  | Middlename | String(40) | Initials. Optional |  | Lastname | String(200) | Optional |  | Email | String(200) |  Required |  | Phone | String(200) |  Required |  | AdditionalInfo | String(4000) |  Optional |  | CampaignName | String(200) |  Optional |      ## Sample message:  ```  {    \"Fullname\": \"John Doe\",    \"Firstname\": \"John\",    \"Middlename\": \"\",    \"Lastname\": \"Doe\",    \"Email\": \"johndoe@test.com\",    \"Phone\": \"0612345678\",    \"AdditionalInfo\": \"This is additional info\",    \"CampaignName\": \"Campaingname\"  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 201 | | Succes |  | 400 | 400016 | Email is empty|  | 400 | 400026 | Invalid Email|  | 400 | 400027 | Phone is empty|  | 401 | 401001 | Unauthorized |  | 409 | 409014 | Crm not active|  | 409 | 409015 | Task not active|  | 409 | 409016 | Error creating lead|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\LeadsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\LeadsResource(); // \Funxtion\Integration\Sportivity\Api\Client\Model\LeadsResource | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->leadsPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LeadsApi->leadsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\LeadsResource**](../Model/LeadsResource.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

