# Funxtion\Integration\Sportivity\Api\Client\LessonsApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**lessonsBookFixedLessonPost**](LessonsApi.md#lessonsbookfixedlessonpost) | **POST** /Lessons/BookFixedLesson | Book a fixed lesson for a customer
[**lessonsBookLessonPost**](LessonsApi.md#lessonsbooklessonpost) | **POST** /Lessons/BookLesson | Book a lesson for a customer
[**lessonsBookLessonPresencePost**](LessonsApi.md#lessonsbooklessonpresencepost) | **POST** /Lessons/BookLessonPresence | Book the presence of a customer for a lesson
[**lessonsBookableLessonsForCustomerCidGet**](LessonsApi.md#lessonsbookablelessonsforcustomercidget) | **GET** /Lessons/BookableLessonsForCustomer/{cid} | Get the bookable lessons of a customer
[**lessonsBookableLessonsGet**](LessonsApi.md#lessonsbookablelessonsget) | **GET** /Lessons/BookableLessons | Get the bookable lessons for a given lesson definition
[**lessonsBookedLessonsCidGet**](LessonsApi.md#lessonsbookedlessonscidget) | **GET** /Lessons/BookedLessons/{cid} | Get the booked lessons of a customer
[**lessonsGet**](LessonsApi.md#lessonsget) | **GET** /Lessons | Retrieve lessons by a ThirdPartyId
[**lessonsLessonsPerTrainerGet**](LessonsApi.md#lessonslessonspertrainerget) | **GET** /Lessons/LessonsPerTrainer | Retrieve the lessons of a trainer
[**lessonsLocationLessonsGet**](LessonsApi.md#lessonslocationlessonsget) | **GET** /Lessons/LocationLessons | Get all lessons of a location
[**lessonsParticipantsLessonDefIDGet**](LessonsApi.md#lessonsparticipantslessondefidget) | **GET** /Lessons/Participants/{LessonDefID} | Get the participants of a lesson

# **lessonsBookFixedLessonPost**
> string lessonsBookFixedLessonPost($body, $xAPITOKEN)

Book a fixed lesson for a customer

Book a fixed lesson for a customer by the customer ID and lesson definition ID.    ### Example URL  https://www.sportivity.com/sportivity-api/Lessons/BookLesson    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## BookFixedLesson  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | CustomerId | Integer | Unique identifier of the customer. Required |  | LessonId | Integer | Unique identifier of the lesson. Required |  | LessonCancelled | Boolean | Default false. Set to true if you want to cancel the fixed lessons. Optional |  | CancelFromDate | String | Default empty. When a date is filled all lessons from this date forward will be cancelled. When empty current date will be used. Optional |    ### Example sample  ```  {    \"CustomerId\": 12345,    \"LessonId\": 12345,    \"LessonCancelled\": true,    \"CancelFromDate\": \"01/01/2021\"  }  ```    Sometimes when you try to book multiple lessons 1 or more lessons can't be booked.  When this happens the response will contain an object 'FailedFixedLessons'.  This object contains all lessons that were not able to be booked. Every failed lesson contains a LessonDate and a Reason why the lesson is unbookable.    All other lessons will still successfully be booked.    ### Successful response sample where a lesson could not be booked  ```  {    \"error\": {      \"HttpStatusCode\": 200    },    \"FailedFixedLessons\": [      {        \"LessonDate\": \"29-01-2021\",        \"Reason\": \"Lesson is fully booked.\"      }    ]  }  ```      ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Success |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | Customer not found |  | 404 | 404005 | No lessondefinition found |

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\LessonsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\Lessons23(); // \Funxtion\Integration\Sportivity\Api\Client\Model\Lessons23 | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->lessonsBookFixedLessonPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LessonsApi->lessonsBookFixedLessonPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\Lessons23**](../Model/Lessons23.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **lessonsBookLessonPost**
> string lessonsBookLessonPost($body, $xAPITOKEN)

Book a lesson for a customer

Book a lesson for a customer by the customer ID and lesson ID.    ### Example URL  https://www.sportivity.com/sportivity-api/Lessons/BookLesson    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## BookLesson  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | CustomerId | Integer | Unique identifier of the customer. Required |  | LessonId | Integer | Unique identifier of the lesson. Required |  | LessonCancelled | Boolean | Default false. Set to true if you want to cancel the booked lesson. Optional |  | NoShow | Boolean | Default false. Set to true if you want to set the status of the customer to no show for the lesson. Optional |  | UseWaitlist | Boolean | Default false. Set to true if you want to put the customer on the waitlist if the lesson is full. Optional |    ### Example sample  ```  {  \"CustomerId\": 123435,  \"LessonId\":1234,  \"LessonCancelled\":false,  \"NoShow\": false,  \"UseWaitlist\": false  }  ```      ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 201 | | Succes |  | 400 | 400014 | Could not parse date|  | 401 | 401001 | Unauthorized |  | 404 | 404001 | Customer not found|  | 404 | 404005 | No lessondefinitions|  | 409 | 409002 | Lesson is fully booked|  | 409 | 409003 | Lesson already booked|  | 409 | 409005 | No lesson to cancel|  | 409 | 409006 | Not allowed to book this lesson, not enough sportcredits|  | 409 | 409007 | Customer is allowed to book lesson but is restricted by company settings.|  | 409 | 409008 | Customer is allowed to book lesson but is restricted by company settings.|  | 409 | 409011 | Cancel restrictions company|  | 500 | 500 | Unexpected error|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\LessonsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\Lessons(); // \Funxtion\Integration\Sportivity\Api\Client\Model\Lessons | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->lessonsBookLessonPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LessonsApi->lessonsBookLessonPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\Lessons**](../Model/Lessons.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **lessonsBookLessonPresencePost**
> string lessonsBookLessonPresencePost($body, $xAPITOKEN)

Book the presence of a customer for a lesson

Book the presence of a customer for a lesson by the customer ID and lesson ID.    ### Example URL  https://www.sportivity.com/sportivity-api/Lessons/BookLessonPresence    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## BookLessonPresence  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | CustomerId | Integer | Unique identifier of the customer. Required |  | LessonId | Integer | Unique identifier of the lesson. Required |    ### Example sample  ```  {  \"CustomerId\": 123435,  \"LessonId\":1234  }  ```      ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No Customer could be found based on the given ID|  | 404 | 404011 | No lessonbooking or wrong bookingstatus|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\LessonsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\Lessons2(); // \Funxtion\Integration\Sportivity\Api\Client\Model\Lessons2 | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->lessonsBookLessonPresencePost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LessonsApi->lessonsBookLessonPresencePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\Lessons2**](../Model/Lessons2.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **lessonsBookableLessonsForCustomerCidGet**
> string lessonsBookableLessonsForCustomerCidGet($cid, $xAPITOKEN, $date, $caption, $fullLessons)

Get the bookable lessons of a customer

Get the bookable lessons for a customer on a specific date    The {cid} path should be replaced by the CustomerId.  Leaving the date empty will result in showing all the bookable lessons for the current date.      ### Example URL  https://www.sportivity.com/sportivity-api/Lessons/BookableLessonsForCustomer/3625118      ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Response processing  You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\".    ## Sample response  ```  {    \"LessonObjects\": [      {        \"Group\": \"Amsterdam Beatrixpark\",        \"Description\": \"Zwanger\",        \"EndTime\": \"12/01/2021 12:30:00\",        \"DayDescription\": \"Tuesday\",        \"LessonId\": 1489474,        \"StartTime\": \"12/01/2021 12:00:00\",        \"Activity\": \"Zwanger Workout\",        \"ThirdPartyId\": \"04c60d69-9c75-48e4-881a-af96fed9a815\",        \"LessonFull\": false,        \"FixedPlacesFull\": false,        \"LessonTrainer\": {          \"Description\": \"Trainer\",          \"Email\": \"sjdklfjldasf@boss.nl\",          \"FirstName\": \"Martijn\",          \"FullName\": \"Martijn van Noord\"        },        \"FreeSpots\": 10,        \"MaximumSpots\": 10      }    ]  }  ```      ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400014 | Wrong format, use date: dd/MM/yyyy|  | 401 | 401001 | Unauthorized |  | 404 | 404001 | Customer not found|  | 404 | 404005 | No lessondefinitions|  | 409 | 409012 | No Active Membership at start lesson|  | 409 | 409013 | Already booked available lessons|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\LessonsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cid = 56; // int | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$date = "date_example"; // string | 
$caption = "caption_example"; // string | 
$fullLessons = false; // bool | 

try {
    $result = $apiInstance->lessonsBookableLessonsForCustomerCidGet($cid, $xAPITOKEN, $date, $caption, $fullLessons);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LessonsApi->lessonsBookableLessonsForCustomerCidGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**|  |
 **xAPITOKEN** | **string**|  | [optional]
 **date** | **string**|  | [optional]
 **caption** | **string**|  | [optional]
 **fullLessons** | **bool**|  | [optional] [default to false]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **lessonsBookableLessonsGet**
> string lessonsBookableLessonsGet($xAPITOKEN, $lessonDefID, $thirdPartyID)

Get the bookable lessons for a given lesson definition

Get the bookable lessons for a given lesson definition or ThirdPartyId    Use either the LessonDefID query string or ThirdPartyID query string.    ### Example URL  https://www.sportivity.com/sportivity-api/Lessons/BookableLessons?ThirdPartyID=iojebwifebiwhebiwvihb    https://www.sportivity.com/sportivity-api/Lessons/BookableLessons?LessonDefID=123456      ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Response processing  You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\".    ## Sample response  ```  {    \"LessonObjects\": [      {        \"Group\": \"Lessengroup\",        \"Description\": \"Testlesson\",        \"EndTime\": \"11/01/2021 12:30:00\",        \"DayDescription\": \"Monday\",        \"LessonId\": 123456,        \"StartTime\": \"11/01/2021 12:00:00\",        \"Activity\": \"Workout\",        \"ThirdPartyId\": \"oijwebsv0oe82bijbdjbviweiuh38iwuef782\",        \"LessonFull\": false,        \"FixedPlacesFull\": false,        \"FreeSpots\": 0,        \"MaximumSpots\": 0      }    ]  }  ```      ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404005 | No lessondefinitions|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\LessonsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$lessonDefID = 56; // int | 
$thirdPartyID = "thirdPartyID_example"; // string | 

try {
    $result = $apiInstance->lessonsBookableLessonsGet($xAPITOKEN, $lessonDefID, $thirdPartyID);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LessonsApi->lessonsBookableLessonsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]
 **lessonDefID** | **int**|  | [optional]
 **thirdPartyID** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **lessonsBookedLessonsCidGet**
> string lessonsBookedLessonsCidGet($cid, $xAPITOKEN, $showWaitinglist)

Get the booked lessons of a customer

Get the booked lessons of a customer by CustomerId  The {cid} path should be replaced with the CustomerId      ### Example URL  https://www.sportivity.com/sportivity-api/Lessons/BookedLessons/1234567    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Response processing  You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\".    ## Sample response  ```  {    \"LessonObjects\": [      {        \"Group\": \"Bootcamp\",        \"Description\": \"Bootcamp\",        \"EndTime\": \"03/11/2020 10:00:00\",        \"DayDescription\": \"Tuesday\",        \"LessonId\": 12345678,        \"StartTime\": \"03/11/2020 09:00:00\",        \"Activity\": \"Bootcamp\",        \"ThirdPartyId\": \"jshbvoeunbsjdvn-qwejnfseuifhv-veunv\",        \"LessonFull\": false,        \"FixedPlacesFull\": false,        \"LessonTrainer\": {          \"Email\": \"trainer@boss.nl\",          \"FirstName\": \"Trainer\",          \"FullName\": \"Trainer Bootcamp\"        }      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:      |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | Customer not found|  | 404 | 404004 | No booked lessons found|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\LessonsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cid = 56; // int | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$showWaitinglist = false; // bool | 

try {
    $result = $apiInstance->lessonsBookedLessonsCidGet($cid, $xAPITOKEN, $showWaitinglist);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LessonsApi->lessonsBookedLessonsCidGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**|  |
 **xAPITOKEN** | **string**|  | [optional]
 **showWaitinglist** | **bool**|  | [optional] [default to false]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **lessonsGet**
> string lessonsGet($xAPITOKEN, $lessonDate, $thirdPartyId)

Retrieve lessons by a ThirdPartyId

Retrieve all lessons by a ThirdPartyId.  The LessonDate parameter should in the format dd/mm/yyyy.  If the LessonDate parameter is left empty you will receive all the lessons with the given ThirdPartyId from the next 3 months.    ### Example URL  https://www.sportivity.com/sportivity-api/Lessons?LessonDate=27%2F01%2F2021&ThirdPartyId=hgfutyrcyuf654eo879f8675df7    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Sample response  ```  {    \"LessonObjects\": [      {        \"Group\": \"Group\",        \"Description\": \"Workout\",        \"EndTime\": \"27/01/2021 12:30:00\",        \"DayDescription\": \"Wednesday\",        \"LessonId\": 1234567,        \"StartTime\": \"27/01/2021 12:00:00\",        \"Activity\": \"Workout\",        \"ThirdPartyId\": \"hgfutyrcyuf654eo879f8675df7\",        \"LessonFull\": false,        \"FixedPlacesFull\": false,        \"LocationName\": \"ACCP:Nieuw Vennep\",        \"LessonTrainer\": {          \"Description\": \"Trainer\",          \"Email\": \"sjdklfjldasf@boss.nl\",          \"FirstName\": \"Trainer\",          \"FullName\": \"Trainer API\"        },        \"FreeSpots\": 0,        \"MaximumSpots\": 0,        \"AdditionalInformation\": \"<p>Additional information here.</p>\\n\"      }    ]  }  ```    The AdditionalInformation is a string filled with HTML text.    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:      |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400014 | Wrong format, use date: dd/MM/yyyy |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | Customer not found |  | 404 | 404005 | No lessondefinitions |

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\LessonsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$lessonDate = "lessonDate_example"; // string | 
$thirdPartyId = "thirdPartyId_example"; // string | 

try {
    $result = $apiInstance->lessonsGet($xAPITOKEN, $lessonDate, $thirdPartyId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LessonsApi->lessonsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]
 **lessonDate** | **string**|  | [optional]
 **thirdPartyId** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **lessonsLessonsPerTrainerGet**
> string lessonsLessonsPerTrainerGet($xAPITOKEN, $email, $lessonDate)

Retrieve the lessons of a trainer

Retrieve all lessons of a trainer on the given date.  The LessonDate parameter should in the format dd/mm/yyyy.  If the Date parameter is left empty you will receive all the lessons from today until next week.    ### Example URL  https://www.sportivity.com/sportivity-api/Lessons/LessonsPerTrainer?Email={Email}    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Response processing  You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\".    ## Sample response  ```  {    \"LessonDate\": \"23/10/2020\",    \"LessonObjects\": [      {        \"Group\": \"Bootcamp\",        \"Description\": \"bootcamp\",        \"EndTime\": \"23/10/2020 13:30:00\",        \"DayDescription\": \"Friday\",        \"LessonId\": 1234567,        \"StartTime\": \"23/10/2020 13:00:00\",        \"Activity\": \"Bootcamp\",        \"ThirdPartyId\": \"hgfutyrcyuf654eo879f8675df7\",        \"LessonFull\": false,        \"FixedPlacesFull\": false,        \"LocationName\": \"Location1\",        \"LessonTrainer\": {          \"Description\": \"\",          \"Email\": \"trainer@test.nl\",          \"FirstName\": \"Trainer\",          \"FullName\": \"Trainer  Boss\"        }      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:      |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | Trainer not found |  | 404 | 404002 | No lessondefinitions |  | 400 | 400001 | Could not parse date.|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\LessonsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$email = "email_example"; // string | 
$lessonDate = "lessonDate_example"; // string | 

try {
    $result = $apiInstance->lessonsLessonsPerTrainerGet($xAPITOKEN, $email, $lessonDate);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LessonsApi->lessonsLessonsPerTrainerGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]
 **email** | **string**|  | [optional]
 **lessonDate** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **lessonsLocationLessonsGet**
> string lessonsLocationLessonsGet($xAPITOKEN, $date)

Get all lessons of a location

Retrieve all lessons of a location on the given date.  The Date parameter should in the format dd/mm/yyyy.  If the Date parameter is left empty you will receive all the lessons from today until next week.    ### Example URL  https://www.sportivity.com/sportivity-api/Lessons/LocationLessons    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Response processing  You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\".    ## Sample response  ```  {    \"LessonDate\": \"23/10/2020\",    \"LessonObjects\": [      {        \"Group\": \"Bootcamp\",        \"Description\": \"bootcamp\",        \"EndTime\": \"23/10/2020 13:30:00\",        \"DayDescription\": \"Friday\",        \"LessonId\": 1234567,        \"StartTime\": \"23/10/2020 13:00:00\",        \"Activity\": \"Bootcamp\",        \"ThirdPartyId\": \"hgfutyrcyuf654eo879f8675df7\",        \"LessonFull\": false,        \"FixedPlacesFull\": false,        \"LocationName\": \"Location1\",        \"LessonTrainer\": {          \"Description\": \"\",          \"Email\": \"\",          \"FirstName\": \"No trainer selected\",          \"FullName\": \"\"        }      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:      |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404005 | No lessondefinitions |  | 404 | 404006 | Could not parse date.|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\LessonsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$date = "date_example"; // string | 

try {
    $result = $apiInstance->lessonsLocationLessonsGet($xAPITOKEN, $date);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LessonsApi->lessonsLocationLessonsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]
 **date** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **lessonsParticipantsLessonDefIDGet**
> string lessonsParticipantsLessonDefIDGet($lessonDefID, $xAPITOKEN)

Get the participants of a lesson

Get the participants of a lesson by LessonDefID    The {LessonDefID} path should be replaced by the Lesson id.    ### Example URL  https://www.sportivity.com/sportivity-api/Lessons/Participants/123456      ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Response processing  You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\".    ## Sample response  ```  {    \"LessonObjects\": [      {        \"Group\": \"Test Group\",        \"Description\": \"Workout\",        \"EndTime\": \"11/01/2021 12:30:00\",        \"DayDescription\": \"Monday\",        \"LessonId\": 123456,        \"StartTime\": \"11/01/2021 12:00:00\",        \"Activity\": \"Workout\",        \"ThirdPartyId\": \"weoijhgbijwbgikjw23456wff35sdfg3\",        \"LessonFull\": false,        \"FixedPlacesFull\": false,        \"LocationName\": \"Location 1\",        \"LessonTrainer\": {          \"Description\": \"Trainer\",          \"Email\": \"sjdklfjldasf@boss.nl\",          \"FirstName\": \"Tess\",          \"FullName\": \"Tess Test\"        },        \"FreeSpots\": 9,        \"MaximumSpots\": 10,        \"Participants\": [          {            \"CustomerId\": 123456,            \"Fullname\": \"John Doe\",            \"Email\": \"info@boss.nl\",            \"Fixed\": false,            \"BookingStatus\": \"Reserved\"          }        ]      }    ]  }  ```      ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404005 | Lesson not found|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\LessonsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$lessonDefID = 56; // int | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->lessonsParticipantsLessonDefIDGet($lessonDefID, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LessonsApi->lessonsParticipantsLessonDefIDGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lessonDefID** | **int**|  |
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

