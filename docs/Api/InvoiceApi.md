# Funxtion\Integration\Sportivity\Api\Client\InvoiceApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**invoiceCreateInvoicePost**](InvoiceApi.md#invoicecreateinvoicepost) | **POST** /Invoice/CreateInvoice | Create an Invoice
[**invoiceCreditPost**](InvoiceApi.md#invoicecreditpost) | **POST** /Invoice/Credit | Credit invoices
[**invoiceDirectDebitGet**](InvoiceApi.md#invoicedirectdebitget) | **GET** /Invoice/DirectDebit | 
[**invoiceGet**](InvoiceApi.md#invoiceget) | **GET** /Invoice | Retrieve invoices
[**invoicePDFGet**](InvoiceApi.md#invoicepdfget) | **GET** /Invoice/PDF | Get the Base64 of a PDF file of an invoice
[**invoicePaidInvoiceGet**](InvoiceApi.md#invoicepaidinvoiceget) | **GET** /Invoice/PaidInvoice | Retrieve paid invoices
[**invoicePartialPaymentPost**](InvoiceApi.md#invoicepartialpaymentpost) | **POST** /Invoice/PartialPayment | Partially pay invoice
[**invoicePost**](InvoiceApi.md#invoicepost) | **POST** /Invoice | Pay invoice
[**invoiceReversePaymentPost**](InvoiceApi.md#invoicereversepaymentpost) | **POST** /Invoice/ReversePayment | Revert a paid invoice

# **invoiceCreateInvoicePost**
> string invoiceCreateInvoicePost($body, $xAPITOKEN)

Create an Invoice

Create an Invoice for a customer    ### Example URL  https://www.sportivity.com/sportivity-api/Invoice/CreateInvoice    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Create Invoice Resource  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | CustomerId | Integer | Unique identifier of the customer. Required  |  | OpenAmount | Integer | The open amount of the invoice. Required  |  | TotalAmount | Integer | The total amount of the invoice. Required |  | ArticleDefinitionId | Integer | Unique identifier of the article definition. Required  |  | DirectDebit | Boolean | Boolean to decide if the invoice needs to be DirectDebit. Required  |      ## Sample message:  ```  {    \"CustomerId\": 123456,    \"OpenAmount\": 10,    \"TotalAmount\": 10,    \"ArticleDefinitionId\": 123456,    \"DirectDebit\": true  }  ```    ## Succesfull response sample  ```  {    \"CustomerId\": 123456,    \"Status\": \"Succes\",    \"InvoiceId\": 123456789  }  ```      ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400022 | Open amound should be 0 or equal to total amount |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | Customer not found|  | 404 | 404020 | No article found|  | 500 | 500 | Unexpected error|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\InvoiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\InvoiceExternal(); // \Funxtion\Integration\Sportivity\Api\Client\Model\InvoiceExternal | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->invoiceCreateInvoicePost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoiceApi->invoiceCreateInvoicePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\InvoiceExternal**](../Model/InvoiceExternal.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **invoiceCreditPost**
> string invoiceCreditPost($body, $xAPITOKEN)

Credit invoices

Credit invoice of a customer by the invoice ID. The API returns a response array containing separate response values for each invoice in the request.    ### Example URL  https://www.sportivity.com/sportivity-api/Invoice/Credit    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Example sample  ```  {      \"CustomerId\": 3051852,      \"Invoices\": [          {              \"InvoiceId\": 123          },          {              \"InvoiceId\": 124          }      ]  }  ```    ## Successful response sample  ```  {      \"CustomerId\": 1234567,      \"Invoices\": [          {              \"InvoiceId\": 123,              \"StatusMessage\": \"Invoice not open\",              \"Succes\": \"false\"          },          {              \"InvoiceId\": 124,              \"StatusMessage\": \"Succes\",              \"Succes\": \"true\"          }      ],      \"error\": {          \"message\": \"Succes\",          \"HttpStatusCode\": 200      }  }  ```    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404019 | No Invoices found|  | 500 | 500 | Unexpected error|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\InvoiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\InvoiceResource(); // \Funxtion\Integration\Sportivity\Api\Client\Model\InvoiceResource | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->invoiceCreditPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoiceApi->invoiceCreditPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\InvoiceResource**](../Model/InvoiceResource.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **invoiceDirectDebitGet**
> string invoiceDirectDebitGet($xAPITOKEN, $invoiceID, $batchSize)



## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404019 | No Invoices found|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\InvoiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$invoiceID = 56; // int | 
$batchSize = 56; // int | 

try {
    $result = $apiInstance->invoiceDirectDebitGet($xAPITOKEN, $invoiceID, $batchSize);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoiceApi->invoiceDirectDebitGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]
 **invoiceID** | **int**|  | [optional]
 **batchSize** | **int**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **invoiceGet**
> string invoiceGet($cid, $xAPITOKEN, $showAll)

Retrieve invoices

Retrieve invoices of a customer by the customer ID.  The {cid} query string should be replaced by \"cid=*CustomerID*\".  Setting the {ShowAll} boolean query string to true will show all invoices. Default false will show only the unpaid invoices.    ### Example URL  https://www.sportivity.com/sportivity-api/Invoice?cid=3625148&ShowAll=false    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"CustomerID\": 1234567,    \"Invoices\": [      {        \"InvoiceNumber\": \"2019-482\",        \"InvoiceID\": 9621,        \"OutstandingAmount\": 25,        \"TotalAmount\": 25,        \"CreateDateInvoiceUTC\": \"14-02-2019 15:55\"      },      {        \"InvoiceNumber\": \"2019-607\",        \"InvoiceID\": 9756,        \"OutstandingAmount\": 25,        \"TotalAmount\": 25,        \"CreateDateInvoiceUTC\": \"19-03-2019 14:36\"      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400021 | No cid |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No customer could be found based on the given ID|  | 404 | 404019 | No Invoices found|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\InvoiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cid = "cid_example"; // string | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$showAll = false; // bool | 

try {
    $result = $apiInstance->invoiceGet($cid, $xAPITOKEN, $showAll);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoiceApi->invoiceGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **string**|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]
 **showAll** | **bool**|  | [optional] [default to false]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **invoicePDFGet**
> string invoicePDFGet($xAPITOKEN, $cid, $invoiceid)

Get the Base64 of a PDF file of an invoice

Retrieve the PDF file of an invoice in base64 format.     In order to retrieve the PDF file you need the customer id and the invoice id.    The invoiceid query string should be replaced by \"invoiceid=*invoiceid*\".  The cid query string should be replaced by \"cid=*cid*\".    ### Example URL  https://www.sportivity.com/sportivity-api/Invoice/PDF?cid=123456&invoiceid=12233    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"PDFString\": \"Base64 string of the PDF file\"  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No customer found|  | 404 | 404019 | No Invoices found|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\InvoiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$cid = 56; // int | 
$invoiceid = 56; // int | 

try {
    $result = $apiInstance->invoicePDFGet($xAPITOKEN, $cid, $invoiceid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoiceApi->invoicePDFGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]
 **cid** | **int**|  | [optional]
 **invoiceid** | **int**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **invoicePaidInvoiceGet**
> string invoicePaidInvoiceGet($xAPITOKEN, $cid)

Retrieve paid invoices

Retrieve paid invoices of a customer by the customer ID.  The {cid} query string should be replaced by \"cid=*CustomerID*\".    ### Example URL  https://www.sportivity.com/sportivity-api/Invoice/cid=1234567    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"CustomerID\": 1234567,    \"Invoices\": [      {        \"InvoiceNumber\": \"2019-482\",        \"InvoiceID\": 9621,        \"OutstandingAmount\": 25,        \"TotalAmount\": 25,        \"CreateDateInvoiceUTC\": \"14-02-2019 15:55\"      },      {        \"InvoiceNumber\": \"2019-607\",        \"InvoiceID\": 9756,        \"OutstandingAmount\": 25,        \"TotalAmount\": 25,        \"CreateDateInvoiceUTC\": \"19-03-2019 14:36\"      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400021 | No cid |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No customer could be found based on the given ID|  | 404 | 404019 | No Invoices found|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\InvoiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$cid = "cid_example"; // string | 

try {
    $result = $apiInstance->invoicePaidInvoiceGet($xAPITOKEN, $cid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoiceApi->invoicePaidInvoiceGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]
 **cid** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **invoicePartialPaymentPost**
> string invoicePartialPaymentPost($body, $xAPITOKEN)

Partially pay invoice

Partially pay an invoice of a customer by the invoice ID.  The {iid} query string should be replaced by \"iid=*InvoiceID*\".  After a successful call the API returns the customer ID.    ### Example URL  https://www.sportivity.com/sportivity-api/Invoice/PartialPayment    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Example sample  ```  {    \"Invoice_PartialPayment\":    {    \"InvoiceId\": 1234567,      \"PartialPaymentAmount\": 10.50      }  }  ```    ## Successful response sample  ```  {      \"CustomerId\": 1212121,      \"error\": {          \"message\": \"Succes\",          \"HttpStatusCode\": 200      }  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404019 | No Invoices found|  | 500 | 500 | Unexpected error|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\InvoiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\InvoiceResource2(); // \Funxtion\Integration\Sportivity\Api\Client\Model\InvoiceResource2 | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->invoicePartialPaymentPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoiceApi->invoicePartialPaymentPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\InvoiceResource2**](../Model/InvoiceResource2.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **invoicePost**
> string invoicePost($iid, $xAPITOKEN, $dca, $paymentmethod)

Pay invoice

Pay invoice of a customer by the invoice ID.  The {iid} query string should be replaced by \"iid=*InvoiceID*\".|      The payment method enumeration parameter should be one of the available values:  Incasso_bureau, iDEAL, Extern  The default payment method = iDEAL      Deprecated: The {dca} query string.      ### Example URL  https://www.sportivity.com/sportivity-api/Invoice?iid=12345&paymentmethod=iDEAL    ### Example URL collection agency  https://www.sportivity.com/sportivity-api/Invoice?iid=12345&paymentmethod=Incasso_bureau    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"error\": {      \"message\": \"Succes\",      \"HttpStatusCode\": 200    }  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400022 | No iid |  | 401 | 401001 | Unauthorized |  | 404 | 404019 | No Invoices found|  | 409 | 409016 | Invoice already paid |  | 500 | 500 | Unexpected error|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\InvoiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$iid = "iid_example"; // string | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$dca = "dca_example"; // string | 
$paymentmethod = "paymentmethod_example"; // string | 

try {
    $result = $apiInstance->invoicePost($iid, $xAPITOKEN, $dca, $paymentmethod);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoiceApi->invoicePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iid** | **string**|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]
 **dca** | **string**|  | [optional]
 **paymentmethod** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **invoiceReversePaymentPost**
> string invoiceReversePaymentPost($xAPITOKEN, $invoiceId)

Revert a paid invoice

Revert a payed invoice of a customer by the InvoiceId.  The InvoiceId query string should be replaced by \"InvoiceId=*InvoiceId*\".    ### Example URL  https://www.sportivity.com/sportivity-api/ReversePayment?InvoiceId=123456    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"CustomerId\": 1234567,    \"Status\": \"Succes\"  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404019 | No Invoices found|  | 409 | 409015 | Invoice not settled|  | 500 | 500 | Unexpected error|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\InvoiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$invoiceId = 56; // int | 

try {
    $result = $apiInstance->invoiceReversePaymentPost($xAPITOKEN, $invoiceId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoiceApi->invoiceReversePaymentPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]
 **invoiceId** | **int**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

