# Funxtion\Integration\Sportivity\Api\Client\QRAccessApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**qRAccessAccessPost**](QRAccessApi.md#qraccessaccesspost) | **POST** /QRAccess/Access | Grant access for a customer using a QRCode

# **qRAccessAccessPost**
> string qRAccessAccessPost($body, $xAPITOKEN)

Grant access for a customer using a QRCode

### Example URL  https://www.sportivity.com/sportivity-api/QRAccess/Access    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Customer Resource  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | CustomerId | Integer | Unique identifier of the customer. Only supplied in the response |  | QRCode | String(200) | QRCode of the access gate |    ## Sample request    ```  {    \"CustomerId\": 1234567,    \"QRCode\": \"SPORTIVITY-HJB487230IH4FFV278YHG2F0H328YH\"  }  ```      ## Sample response for a successful access request  ```  {    \"Access\": true,    \"Message\": \"Uw bezoek is geregistreerd.\\nFijne training.\",    \"Color\": \"Green\"  }  ```    ## Sample response for a successfull request where access isn't granted  ```  {    \"Access\": false,    \"Message\": \"Registratie niet gelukt. \\nVerkeerde tijdzone of dubbele registratie. \\nNeem contact op met 1 van onze medewerkers.\",    \"Color\": \"Purple\"  }  ```    Possible colors and their meanings:  Green = Access granted  Red = Access denied  Purple = The customer does not have access at this specific time.    Possible reasons for a purple response:  1. The user tried to get access to fast after last granted access  2. The person is not allowed to enter during this time slot      ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404001| No Customer found|  | 404 || Card not found|  | 404 || Could not find QR code|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\QRAccessApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\QRBody(); // \Funxtion\Integration\Sportivity\Api\Client\Model\QRBody | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->qRAccessAccessPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QRAccessApi->qRAccessAccessPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\QRBody**](../Model/QRBody.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

