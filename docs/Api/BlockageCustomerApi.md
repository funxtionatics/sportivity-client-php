# Funxtion\Integration\Sportivity\Api\Client\BlockageCustomerApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blockageCustomerPassNrGet**](BlockageCustomerApi.md#blockagecustomerpassnrget) | **GET** /BlockageCustomer/{PassNr} | Get Blockages of customer with PassNumber

# **blockageCustomerPassNrGet**
> string blockageCustomerPassNrGet($passNr, $xAPITOKEN)

Get Blockages of customer with PassNumber

Get the gate blockages of a customer by the passnumer.  The {PassNr} parameter should be replaced with the pass number of the customer  If the pass number is unknown, then use a customerid instead.    ### Example URL  https://www.sportivity.com/sportivity-api/BlockageCustomer/123456    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"CustomerID\": 1234567,    \"Token\": \"L9wioejfghwighbiwqbviqebievbiqehbifugh\",    \"APIName\": \"BlockageCustomer/get_BlockageCustomer\",    \"PassNumber\": \"12345678\",    \"Blockage\": \"true\",    \"Blockages\": [      {        \"GateBlockage\": true,        \"FinancialBlockage\": false,        \"Descripton\": \"Bevriezing\",        \"AccesBlockDateStart\": \"2020-12-15T00:00:00.000Z\",        \"AccesBlockDateEnd\": \"2020-12-16T00:00:00.000Z\"      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No customer could be found |

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\BlockageCustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$passNr = "passNr_example"; // string | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->blockageCustomerPassNrGet($passNr, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BlockageCustomerApi->blockageCustomerPassNrGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **passNr** | **string**|  |
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

