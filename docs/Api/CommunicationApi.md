# Funxtion\Integration\Sportivity\Api\Client\CommunicationApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**communicationPost**](CommunicationApi.md#communicationpost) | **POST** /Communication | Upload a PDF or Note

# **communicationPost**
> string communicationPost($body, $xAPITOKEN)

Upload a PDF or Note

With this call you can post a note, email or PDF to the customer.  The Email and PDF should be a string of the Base64 format.    ### Example URL  https://www.sportivity.com/sportivity-api/Communication    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"      ## Sample message:  ```  {    \"CustomerId\": 1234567,    \"Note\": \"string\",    \"Email\": \"Base64\",    \"EmailSubject\": \"Subject of the email\",    \"PDF1\": \"Base64\",    \"PDF2\": \"Base64\",    \"PDF3\": \"Base64\",    \"PDF1Description\": \"Description of PDF1\",    \"PDF2Description\": \"Description of PDF2\",    \"PDF3Description\": \"Description of PDF3\",    \"NotificationID\": \"string\",      }  ```    ### Successful response sample:  ```  {    \"CustomersId\": 1234567  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No customer could be found based on the given ID|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\CommunicationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\Communication(); // \Funxtion\Integration\Sportivity\Api\Client\Model\Communication | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->communicationPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommunicationApi->communicationPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\Communication**](../Model/Communication.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

