# Funxtion\Integration\Sportivity\Api\Client\CustomersApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customersBookPresenceCidPost**](CustomersApi.md#customersbookpresencecidpost) | **POST** /Customers/BookPresence/{cid} | Book the presence of a customer
[**customersByTypeTypeGet**](CustomersApi.md#customersbytypetypeget) | **GET** /Customers/ByType/type | Retrieve a customer by ID, Passnumber or Email
[**customersCidGet**](CustomersApi.md#customerscidget) | **GET** /Customers/{cid} | Retrieve customer by ID
[**customersGroupCustomerIdPost**](CustomersApi.md#customersgroupcustomeridpost) | **POST** /Customers/Group/{CustomerId} | Create a new group or add a customer to a group
[**customersNewCustomersFromDateGet**](CustomersApi.md#customersnewcustomersfromdateget) | **GET** /Customers/NewCustomers/{FromDate} | Get new customers from given date
[**customersPost**](CustomersApi.md#customerspost) | **POST** /Customers | Submit a new customer

# **customersBookPresenceCidPost**
> string customersBookPresenceCidPost($cid, $xAPITOKEN, $mid, $useRules)

Book the presence of a customer

The {cid} parameter should be replaced with the customer ID  The mid query string is the membershipId, this one is optional. If this is empty a booking will be placed on all the memberships of the customer.  The UseRules query string is false by default, when set to true it will only book presence on the membership if the customer has visits left on the membership.    ### Example URL  https://www.sportivity.com/sportivity-api/Customers/BookPresence/123456?mid=1234567&UseRules=false    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"      ## Response processing    On a success you will receive a status code 200 and the response body will be empty.    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No customer could be found based on the given ID|  | 404 | 404007 | Memberships not found|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cid = 56; // int | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$mid = 56; // int | 
$useRules = false; // bool | 

try {
    $result = $apiInstance->customersBookPresenceCidPost($cid, $xAPITOKEN, $mid, $useRules);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->customersBookPresenceCidPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**|  |
 **xAPITOKEN** | **string**|  | [optional]
 **mid** | **int**|  | [optional]
 **useRules** | **bool**|  | [optional] [default to false]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customersByTypeTypeGet**
> string customersByTypeTypeGet($xAPITOKEN, $cid, $passnr, $email, $mem)

Retrieve a customer by ID, Passnumber or Email

Retrieve a customer using either the customer ID, Passnumber or Email.    This call prioritizes the usage of the query string in the following order:  1. cid  2. passnr  3. email    If multiple of these query strings are filled only the prioritized value will be used.    To get the memberships of a customer the Mem parameter should be one of the following string values:  'True'  'true'   't'   'T'      ### Example URL  https://www.sportivity.com/sportivity-api/Customers/ByType/type?cid=123456&mem=T    https://www.sportivity.com/sportivity-api/Customers/ByType/type?passnr=12141&mem=T    https://www.sportivity.com/sportivity-api/Customers/ByType/type?email=exaple@email.com&mem=T    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Response processing  You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\".    ## Sample response  ```  {    \"CustomerResources\": [      {        \"CustomerId\": 123456789,        \"PassNumber\": \"a3456drh5\",        \"Title\": \"Dhr.\",        \"Initials\": \"J\",        \"FirstName\": \"John\",        \"MiddleName\": \"\",        \"LastName\": \"Doe\",        \"Email\": \"test@boss.nl\",        \"PhonePrivate\": \"+31612345678\",        \"Address\": \"Poseidon Street\",        \"HouseNumber\": \"11\",        \"HouseNumberExtension\": \"\",        \"Zipcode\": \"1234ID\",        \"City\": \"Atlantis\",        \"BirthDate\": \"01/01/1999\",        \"CustomerHasActiveMemberships\": true,        \"TermsAccepted\": true,        \"Country\": \"Nederland\",        \"CompanyLocationId\": 123456,        \"CompanyLocationName\": \"Locatie\",        \"CustomerMemberships\": [          {            \"MembershipId\": 12345678,            \"Description\": \"Abonnement\",            \"MembershipAmount\": 10,            \"StartDate\": \"09-11-2021\",            \"ContractEndDate\": \"01-02-2022\",            \"PaymentEndDate\": \"09-12-2021\",            \"AccessEndDate\": \"09-12-2021\",            \"FutureMembership\": false,            \"UnlimitedVisits\": true,            \"CancelledPerDate\": \"01-02-2022\",            \"VisitsLeft\": 999,            \"Terminated\": true,            \"MembershipDefinitionId\": 123456          }        ],        \"CustomerLocations\": [          {            \"LocationId\": 123456,            \"LocationName\": \"Locatie\",            \"Status\": \"Active\"          }        ]      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400025 | None of the required querystrings are filled|  | 401 | 401001 | Unauthorized |  | 404 | 404001 | Customer not found with given cid|  | 404 | 404024| Card not found|  | 404 | 404025 | Customer not found with given passnumber|  | 404 | 404026 | Customer not found with given email|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$cid = 56; // int | 
$passnr = "passnr_example"; // string | 
$email = "email_example"; // string | 
$mem = "mem_example"; // string | 

try {
    $result = $apiInstance->customersByTypeTypeGet($xAPITOKEN, $cid, $passnr, $email, $mem);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->customersByTypeTypeGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]
 **cid** | **int**|  | [optional]
 **passnr** | **string**|  | [optional]
 **email** | **string**|  | [optional]
 **mem** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customersCidGet**
> string customersCidGet($cid, $xAPITOKEN, $mem)

Retrieve customer by ID

Retrieve a customer using the customer ID.  The {cid} parameter should be replaced with the customer ID    To get the memberships of a customer the Mem parameter should be one of the following string values:  'True'  'true'   't'   'T'    ### Example URL  https://www.sportivity.com/sportivity-api/Customers/123456?Mem=true    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"      ## Sample response  ```  {    \"PassNumber\": \"eafea84548\",    \"FirstName\": \"John\",    \"MiddleName\": \"\",    \"LastName\": \"Doe\",    \"Email\": \"test@boss.nl\",    \"PhoneMobile\": \"\",    \"PhonePrivate\": \"\",    \"Address\": \"Poseidon Street\",    \"HouseNumber\": \"11\",    \"Zipcode\": \"1234ID\",    \"City\": \"Atlantis\",    \"BirthDate\": \"01/01/1999\",    \"IBAN\": \"\",    \"BIC\": \"\",    \"Language\": \"Nederlands\",    \"CustomerId\": 12345678,    \"Country\": \"Nederland\",    \"Title\": \"Mevr.\",    \"Initials\": \"J\",    \"HouseNumberExtension\": \"\",    \"CustomerMemberships\": [      {        \"MembershipId\": 1234567,        \"Description\": \"Membership\",        \"MembershipAmount\": 0,        \"StartDate\": \"23-04-2020\",        \"ContractEndDate\": \"26-08-2020\",        \"PaymentEndDate\": \"26-08-2020\",        \"AccessEndDate\": \"24-08-2020\",        \"FutureMembership\": false,        \"UnlimitedVisits\": true,        \"CancelledPerDate\": \"\",        \"VisitsLeft\": 999,        \"Terminated\": false      }    ],    \"OptIn\": \"True\"  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No customer could be found based on the given ID|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cid = 56; // int | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$mem = "mem_example"; // string | 

try {
    $result = $apiInstance->customersCidGet($cid, $xAPITOKEN, $mem);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->customersCidGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**|  |
 **xAPITOKEN** | **string**|  | [optional]
 **mem** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customersGroupCustomerIdPost**
> string customersGroupCustomerIdPost($customerId, $xAPITOKEN, $groupname, $groupCustomerId)

Create a new group or add a customer to a group

This call can be used for both adding a customer to an existing group and creating a new group. Use the {CustomerId} to select the customer you want to create a group for or add to an existing group.  Use one of the following for creating a group or adding the customer to an existing group.    To create a new group you will need to give the CustomerId of the customer that will own the new group and the group name.    ### Example URL creating a new group:  https://www.sportivity.com/sportivity-api/Customers/Group/[CustomerId}?Groupname={Groupname}      To add a customer to an existing group you need to give the CustomerId of the customer you want to add to the group and the CustomerId of a customer that is already a member of this group.    ### Example URL adding a customer to an existing group:  https://www.sportivity.com/sportivity-api/Customers/Group/{CustomerId?Group_CustomerId={Group_CustomerId}    ### Important note  When adding a membership that is supposed to have a group discount, the member first has to be part of a group. If the membership is added before the member is in a group the first period will be at full price.      ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"      ## Response processing    On a success you will receive a status code 200 and the response body will be empty.    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Customer added to group |  | 201 | | Succesfully created new group |  | 400 | 400001 | Customer is already in a group |  | 400 | 400002 | Please only use one option between Groupname for creating a new group or Group_CustomerId for adding to an existing group. |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | Customer not found |  | 404 | 404002 | Group not found |  | 500 | 500001 | Unexpected error. New group could not be created.|  | 500 | 500002 | Unexpected error. The customer could not be added to the group.|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$customerId = 56; // int | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$groupname = "groupname_example"; // string | 
$groupCustomerId = 56; // int | 

try {
    $result = $apiInstance->customersGroupCustomerIdPost($customerId, $xAPITOKEN, $groupname, $groupCustomerId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->customersGroupCustomerIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **int**|  |
 **xAPITOKEN** | **string**|  | [optional]
 **groupname** | **string**|  | [optional]
 **groupCustomerId** | **int**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customersNewCustomersFromDateGet**
> string customersNewCustomersFromDateGet($fromDate, $xAPITOKEN)

Get new customers from given date

Retrieve new customers from a given date.  The {FromDate} parameter should be replaced with the date from which you want the new customers.    You will receive all customers that were added from the {FromDate} until now.  The date format is dd/mm/yyyy.    ### Example URL  https://www.sportivity.com/sportivity-api/Customers/NewCustomers/01%2F01%2F2020    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"      ## Sample response  ```  {    \"GetNewCustomersInfos\": [      {        \"CustomerName\": \"John Doe\",        \"CustomerId\": 123456,        \"CreateDate\": \"12-3-20 8:52\",        \"CompanyLocationName\": \"Location 1\",        \"CurrentStatus\": \"Actief\"      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400006 | Could not parse date.|  | 401 | 401001 | Unauthorized |  | 404 | 404013 | No new Customers|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$fromDate = "fromDate_example"; // string | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->customersNewCustomersFromDateGet($fromDate, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->customersNewCustomersFromDateGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fromDate** | **string**|  |
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customersPost**
> string customersPost($body, $xAPITOKEN)

Submit a new customer

Supply a Customer resource in the body of the HTTP POST request.    ### Example URL  https://www.sportivity.com/sportivity-api/Customers    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Customer Resource  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | CustomerId | Integer | Unique identifier of the customer. Only supplied in the response |  | PassNumber | String(200) | Access card number. Optional |  | FirstName | String(200) | First name of the customer. Required |  | Initials | String(40) | Initials. Optional |  | MiddleName | String(200) | Optional |  | LastName | String(200) |  Required |  | Email | String(200) |  Required |  | Country | Enumeration |  Optional. See below for enumeration options |  | PhoneMobile | String(200) |  Optional |  | PhonePrivate | String(200) |  Optional |  | Gender | Enumeration |  Optional. See below for enumeration options |  | Zipcode | String(200) |  Required |  | City | String(200) |  Required |  | Address | String(200) |  Required |  | HouseNumber | String(200) |  Required |  | HouseNumberExtension | String(50) |  Optional |  | BirthDate | Date |  Required. Important: the date format needs to be specified as dd/MM/yyyy, for example: 31/12/1999 |  | IBAN | String(200) |  Optional. Bank account number |  | BIC | String(200) |  Optional. Note: If IBAN attribute is supplied, the BIC attribute is required |  | Language| Enumeration |   Optional. See below for enumeration options |  | UseValidation | Boolean | Standard false, setting to true will use validation check. Optional.  |  | IsValidated | Boolean | Standard false, setting to true will use insert the customer after validation. Optional.  |      ### Enumeration Country  - Nederland  - South_Africa  - Belgie  - Deutschland  - France  - Swiss  - Singapore  - Hongkong  - Austria  - Sweden  - USA  - Japan  - Kenia  - Noorwegen    ### Enumeration Gender  - Male  - Female    ### Enumeration Language  Currently the only 2 supported languages are Dutch and English:  - nl_NL  - en_US        ## Sample message:  ```  {   \"Initials\": \"\",   \"PassNumber\": \"\",   \"FirstName\": \"John\",   \"MiddleName\": \"\",   \"LastName\": \"Doe\",   \"Email\": \"johndoe@foo.bar\",   \"Country\": \"South_Africa\",   \"PhoneMobile\": \"\",   \"PhonePrivate\": \"\",   \"Gender\": \"\",   \"Zipcode\" : \"1234ID\",   \"City\": \"Atlantis\",   \"Address\" : \"Poseidon Street\",   \"HouseNumber\" : \"11\",   \"HouseNumberExtension\" : \"\",   \"BirthDate\": \"31/12/1999\",   \"IBAN\": \"NL16ABNA0839495696\",   \"BIC\": \"33\",   \"Language\": \"nl_NL\"  }  ```    ## Using validation check  To use validation set the UseValidation boolean in the body to true,    If UseValidation is true and IsValidated is false, the response will contain a ValidationCode. This code is also sent to the customers email address.     After you have verified the user with the code make the same post call with the UseValidation and IsValidated set to true.  Now the customer will be inserted to the database.    ## Sample of validation:  ```  {   \"Initials\": \"\",   \"PassNumber\": \"\",   \"FirstName\": \"John\",   \"MiddleName\": \"\",   \"LastName\": \"Doe\",   \"Email\": \"johndoe@foo.bar\",   \"Country\": \"South_Africa\",   \"PhoneMobile\": \"\",   \"PhonePrivate\": \"\",   \"Gender\": \"\",   \"Zipcode\" : \"1234ID\",   \"City\": \"Atlantis\",   \"Address\" : \"Poseidon Street\",   \"HouseNumber\" : \"11\",   \"HouseNumberExtension\" : \"\",   \"BirthDate\": \"31/12/1999\",   \"IBAN\": \"NL16ABNA0839495696\",   \"BIC\": \"33\",   \"Language\": \"nl_NL\"   \"UseValidation\": true,   \"IsValidated\": false  }  ```  ## Sample of a validation response:  ```  {      \"ValidationCode\": 1234  }  ```      ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 201 | | Succes |  | 204 | | The user already exists |  | 400 | 400001 | No birthdate specified. Please use following format for birthdate: \"31/12/2016\"|  | 400 | 400002 | FirstName is required|  | 400 | 400003 | Last name is required|  | 400 | 400004 | ZipCode is required|  | 400 | 400005 | HouseNumber is required|  | 400 | 400006 | Address is required|  | 400 | 400007 | City is required|  | 400 | 400008 | IBAN not valid|  | 400 | 400010 | IBAN not valid|  | 400 | 400011 | Language not filled|  | 400 | 400012 | Unsupported language|  | 400 | 400013 | Unsupported country|  | 401 | 401001 | Unauthorized |

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\CustomerResource(); // \Funxtion\Integration\Sportivity\Api\Client\Model\CustomerResource | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->customersPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->customersPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\CustomerResource**](../Model/CustomerResource.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

