# Funxtion\Integration\Sportivity\Api\Client\TogetherEntranceApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**togetherEntrancePost**](TogetherEntranceApi.md#togetherentrancepost) | **POST** /TogetherEntrance | Post a together entrance for a customer

# **togetherEntrancePost**
> string togetherEntrancePost($body, $xAPITOKEN)

Post a together entrance for a customer

### Example URL  https://www.sportivity.com/sportivity-api/TogetherEntrance    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"      ## Sample message:  ```  {    \"CustomersID\": 12345678,    \"FullnameGuest\": \"Test Meneer\",    \"EmailGuest\": \"test@mail.com\",    \"MobilePhoneGuest\": \"+31612345678\",    \"DateVisitGuest\": \"26/10/2021\"  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\".       ## Successful response sample:  ```  {    \"CustomersID\": 12345678,    \"LeadAdded\": \"true\"  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400002 | CancelledPerdate must be bigger than StartDate|  | 401 | 401001 | Unauthorized |  | 400 | 404002 | No Customer could be found based on the given ID|  | 400 | 400003 | No DateVisitGuest|  | 400 | 400004 | No EmailGuest|  | 400 | 400005 | No FullnameGuest|  | 400 | 400006 | Not a valid date|  | 500 | 500 | Error parsing JSON|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\TogetherEntranceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\TogetherVisit(); // \Funxtion\Integration\Sportivity\Api\Client\Model\TogetherVisit | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->togetherEntrancePost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TogetherEntranceApi->togetherEntrancePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\TogetherVisit**](../Model/TogetherVisit.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

