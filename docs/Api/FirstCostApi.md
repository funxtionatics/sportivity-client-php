# Funxtion\Integration\Sportivity\Api\Client\FirstCostApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**firstCostGet**](FirstCostApi.md#firstcostget) | **GET** /FirstCost | Get the cost of the first term of a membership

# **firstCostGet**
> string firstCostGet($mid, $startdate, $xAPITOKEN, $action)

Get the cost of the first term of a membership

Get the cost of the first term of a membership.  The {mid} query string should be replaced by \"mid=*MembershipDefinitionID*\".  The {startdate} query string should be replaced by \"startdate=*Startdate*\".  The {action} query string should be replaced by boolean true, false or empty when false.    The dateformat is dd/mm/yyyy  The default value for Action = false    ### Example URL  https://www.sportivity.com/sportivity-api/FirstCost?mid=1234&startdate=01/10/2019&action=true    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"CompanyLocationName\": \"Nieuw Vennep\",    \"MembershipDescription\": \"Fitness onbeperkt\",    \"Family\": false,    \"CompanylocationID\": \"13000\",    \"Token\": \"eQCbIs6ivSLr3okD8Y=\",    \"APIName\": \"GetMembershipPrice\",    \"error\": {      \"HttpStatusCode\": 200    },    \"AmountDeposit1\": 0,    \"AmountDeposit2\": 0,    \"AmountDeposit3\": 0,    \"Deposit1\": \"AA groen\",    \"Deposit2\": \"AA oranje\",    \"Deposit3\": \"Aquarius\",    \"MembershipDefinitionID\": 1234,    \"MembershipFirstCosts\": 35,    \"StartDate\": \"01/10/2019\",    \"MembershipFirstCostsWithDeposits\": 35  }  ```    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 400 | No valid input |  | 401 | 401001 | Unauthorized |  | 404 | 404002 | No membership could be found based on the given ID|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\FirstCostApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mid = "mid_example"; // string | 
$startdate = "startdate_example"; // string | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$action = false; // bool | 

try {
    $result = $apiInstance->firstCostGet($mid, $startdate, $xAPITOKEN, $action);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FirstCostApi->firstCostGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mid** | **string**|  | [optional]
 **startdate** | **string**|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]
 **action** | **bool**|  | [optional] [default to false]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

