# Funxtion\Integration\Sportivity\Api\Client\MembershipDefinitionApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**membershipDefinitionGet**](MembershipDefinitionApi.md#membershipdefinitionget) | **GET** /MembershipDefinition | Retrieve all membership definitions

# **membershipDefinitionGet**
> string membershipDefinitionGet($xAPITOKEN)

Retrieve all membership definitions

Retrieve all membership definitions.    ### Example URL  https://www.sportivity.com/sportivity-api/MembershipDefinition      ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"      ## Response processing  You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an “Error” object with attributes “code” and \"message\".      ## Successful response sample  ```  {    \"MembershipDefinitions\": [      {        \"Description\": \"Shape abonnement\",        \"LocationName\": \"ACCP:Nieuw Vennep\",        \"LocationId\": 13000,        \"Active\": false,        \"MembershipId\": 8721,        \"IsCompanyMemberhip\": false,        \"CostCentreName\": \"Sport\",        \"CostCentreNumber\": 3,        \"AmountDeposit1\": 0,        \"AmountDeposit2\": 0,        \"AmountDeposit3\": 0,        \"Deposit1\": \"wbsom\",        \"Deposit2\": \"AA oranje\",        \"Deposit3\": \"Aquarius\",        \"LId_CMDId\": \"\",        \"OnlyOnce\": false,        \"Amount\": 15,        \"PaymentDurationPeriod\": \"Maand/maanden\",        \"PaymentDuration\": 1,        \"UnlimitedVisits\": false,        \"NumberOfVisits\": 10,        \"Action\": false,        \"ActionDescription\": \"\"      },      {        \"Description\": \"AM 9 pw\",        \"LocationName\": \"ACCP:Nieuw Vennep\",        \"LocationId\": 13000,        \"Active\": true,        \"MembershipId\": 4243,        \"IsCompanyMemberhip\": false,        \"AmountDeposit1\": 0,        \"AmountDeposit2\": 0,        \"AmountDeposit3\": 0,        \"Deposit1\": \"wbsom\",        \"Deposit2\": \"AA oranje\",        \"Deposit3\": \"Aquarius\",        \"LId_CMDId\": \"\",        \"OnlyOnce\": false,        \"Amount\": 22.99,        \"PaymentDurationPeriod\": \"Maand/maanden\",        \"PaymentDuration\": 1,        \"UnlimitedVisits\": true,         \"Action\": false,        \"ActionDescription\": \"\"      },      {        \"Description\": \"Kortings abo 1\",        \"LocationName\": \"ACCP:Nieuw Vennep\",        \"LocationId\": 13000,        \"Active\": false,        \"MembershipId\": 4577,        \"IsCompanyMemberhip\": false,        \"CostCentreName\": \"Horeca\",        \"CostCentreNumber\": 1,        \"AmountDeposit1\": 0,        \"AmountDeposit2\": 0,        \"AmountDeposit3\": 0,        \"Deposit1\": \"wbsom\",        \"Deposit2\": \"AA oranje\",        \"Deposit3\": \"Aquarius\",        \"LId_CMDId\": \"\",        \"OnlyOnce\": true,        \"Amount\": 308,        \"PaymentDurationPeriod\": \"Week/weken\",        \"PaymentDuration\": 16,        \"UnlimitedVisits\": true,        \"Action\": true,        \"ActionDescription\": \"1e maand gratis\"      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404002 | Membership definition not found|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\MembershipDefinitionApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->membershipDefinitionGet($xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MembershipDefinitionApi->membershipDefinitionGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

