# Funxtion\Integration\Sportivity\Api\Client\WebhooksApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**webhooksCustomerUpdateGet**](WebhooksApi.md#webhookscustomerupdateget) | **GET** /Webhooks/CustomerUpdate | Receiving customer updates through the webhook
[**webhooksDebtCollectionGet**](WebhooksApi.md#webhooksdebtcollectionget) | **GET** /Webhooks/DebtCollection | Receiving debt collection through the webhook
[**webhooksMembershipUpdateGet**](WebhooksApi.md#webhooksmembershipupdateget) | **GET** /Webhooks/MembershipUpdate | Receiving membership updates through the webhook
[**webhooksSendVisitsGet**](WebhooksApi.md#webhookssendvisitsget) | **GET** /Webhooks/SendVisits | Receiving visits through the webhook

# **webhooksCustomerUpdateGet**
> string webhooksCustomerUpdateGet()

Receiving customer updates through the webhook

## With this webhook you will receive a notification that the customer has been updated.    ### Webhook endpoint URL  In order to start receiving calls through the webhook an endpoint needs to be given.  Note: All webhook calls for a location will be sent to the same endpoint.    You will receive the token and a type. The token value will give you the X-API-TOKEN of the location an update has happened. This token is also needed to retrieve the customer updates.    In the type value you will see what kind of webhook you receive. This will tell you what call to make in order to actually retrieve the changed data. In this case you will receive type \"CustomersUpdate\".    ### Please refer to CustomersUpdate for the next step.    ## Sample of a received CustomerUpdate call  ```  {    \"token\": \"ijbfiuwbefijbw978ghvoj34nfo2w8ery2u3bfn9u2h9w784ghf\",    \"type\": \"CustomersUpdate\"  }  ```    ## Sample of a received CustomerNew call  ```  {    \"token\": \"ijbfiuwbefijbw978ghvoj34nfo2w8ery2u3bfn9u2h9w784ghf\",    \"type\": \"CustomersNew\",    \"CustomerId\": 12345678  }  ```

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\WebhooksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->webhooksCustomerUpdateGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->webhooksCustomerUpdateGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **webhooksDebtCollectionGet**
> string webhooksDebtCollectionGet()

Receiving debt collection through the webhook

## With this webhook you will receive the debt collection of a location.    Note; you will only receive the invoices with the status \"Openstaand\"   and collection process \"Sent to collection agency\"    ### Webhook endpoint URL  In order to start receiving calls through the webhook an endpoint needs to be given to B.O.S.S.  Note: All webhook calls for a location will be sent to the same endpoint.    Every day you will receive a list for the location and all their debt collections.    ## Sample of a received DebtCollection call  ```  {    \"LocationName\": \"Location\",    \"LocationId\": 13000,    \"CustomerIncassos\": [      {        \"FirstName\": \"Thea\",        \"MiddleName\": \"\",        \"LastName\": \"Test\",        \"Email\": \"info@boss.nl\",        \"PhoneMobile\": \"133713371\",        \"PhonePrivate\": \"0612345678\",        \"Gender\": \"Dhr.\",        \"Address\": \"Frankweg\",        \"HouseNumber\": \"41\",        \"Zipcode\": \"2153pd\",        \"City\": \"Nieuw-Vennep\",        \"BirthDate\": \"01-10-1992\",        \"IBAN\": \"NL13ABNA7232510543\",        \"BIC\": \"ABNANL2A\",        \"Language\": \"Nederlands\",        \"CustomerId\": 1234567,        \"Country\": \"Nederland\",        \"Initials\": \"T.T.\",        \"HouseNumberExtension\": \"A\",        \"InvoicesIncassos\": [          {            \"InvoiceNumber\": \"2019-123\",            \"InvoiceID\": 1234,            \"OutstandingAmount\": 70.1,            \"TotalAmount\": 70.1,            \"CreateDateInvoiceUTC\": \"19-03-2019\"          }        ]      }    ]  }  ```

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\WebhooksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->webhooksDebtCollectionGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->webhooksDebtCollectionGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **webhooksMembershipUpdateGet**
> string webhooksMembershipUpdateGet()

Receiving membership updates through the webhook

## With this webhook you will receive a notification that the membership of a customer has been updated.    ### Webhook endpoint URL  In order to start receiving calls through the webhook an endpoint needs to be given.  Note: All webhook calls for a location will be sent to the same endpoint.    You will receive the token and a type. The token value will give you the X-API-TOKEN of the location an update has happened. This token is also needed to retrieve the membership updates.    In the type value you will see what kind of webhook you receive. This will tell you what call to make in order to actually retrieve the changed data. In this case you will receive type \"MembershipUpdate\".    ### Please refer to MembershipsUpdate for the next step.    ## Sample of a received MembershipUpdate call  ```  {    \"token\": \"ijbfiuwbefijbw978ghvoj34nfo2w8ery2u3bfn9u2h9w784ghf\",    \"type\": \"MembershipUpdate\"  }  ```

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\WebhooksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->webhooksMembershipUpdateGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->webhooksMembershipUpdateGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **webhooksSendVisitsGet**
> string webhooksSendVisitsGet()

Receiving visits through the webhook

##With this webhook you will receive the visits of a location.    ### Webhook endpoint URL  In order to start receiving calls through the webhook an endpoint needs to be given.    ## Sample of a received visit call  ```  {    \"Token\": \"ierurgfb830hj20389h9iu329c83892ffn283hc923fdn8uow\",    \"EntryDate\": \"30-10-2020 18:09\",    \"Customersid\": 1234567,    \"Result\": \"Handmatig\",    \"NumberOfVisits\": 1,    \"Gate\": \"Poort\"  }  ```

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\WebhooksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->webhooksSendVisitsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->webhooksSendVisitsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

