# Funxtion\Integration\Sportivity\Api\Client\CustomerCreditsApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerCreditsCidAmountPost**](CustomerCreditsApi.md#customercreditscidamountpost) | **POST** /CustomerCredits/{cid}/{amount} | Add Customer credits by CustomerId
[**customerCreditsGet**](CustomerCreditsApi.md#customercreditsget) | **GET** /CustomerCredits | Retrieve customer credits by customer ID

# **customerCreditsCidAmountPost**
> string customerCreditsCidAmountPost($cid, $amount, $xAPITOKEN)

Add Customer credits by CustomerId

Add customer credits by Customer ID.  The {cid} path should be replaced by \"*CustomerID*\".  The {amount} path should be replaced by \"*Amount*\".    ### Example URL  https://www.sportivity.com/sportivity-api/CustomerCredits/1234567/10      When adding credits the invoice will automatically be set to paid by IDeal.      ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {      \"CustomerId\": 1234567,      \"CustomerCredits\": 10  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No customer could be found based on the given ID|  | 404 | 404026 | No credit product exist on this location|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\CustomerCreditsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cid = 56; // int | 
$amount = 1.2; // float | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->customerCreditsCidAmountPost($cid, $amount, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerCreditsApi->customerCreditsCidAmountPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**|  |
 **amount** | **float**|  |
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerCreditsGet**
> string customerCreditsGet($cid, $xAPITOKEN)

Retrieve customer credits by customer ID

Retrieve the customers credits by Customer ID.  The {cid} query string should be replaced by \"cid=*CustomerID*\".    ### Example URL  https://www.sportivity.com/sportivity-api/CustomerCredits?cid=1234567    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {      \"CustomerId\": 1234567,      \"CustomerCredits\": 10  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No customer could be found based on the given ID|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\CustomerCreditsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cid = "cid_example"; // string | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->customerCreditsGet($cid, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerCreditsApi->customerCreditsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **string**|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

