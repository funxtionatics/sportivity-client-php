# Funxtion\Integration\Sportivity\Api\Client\SportsCreditsApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sportsCreditsCidAmountPost**](SportsCreditsApi.md#sportscreditscidamountpost) | **POST** /SportsCredits/{cid}/{amount} | Add SportsCredits to a customer by CustomerID
[**sportsCreditsCidGet**](SportsCreditsApi.md#sportscreditscidget) | **GET** /SportsCredits/{cid} | Retrieve sports credits of a customer by the customer ID

# **sportsCreditsCidAmountPost**
> string sportsCreditsCidAmountPost($cid, $amount, $xAPITOKEN)

Add SportsCredits to a customer by CustomerID

Add SportsCredits to a customer by CustomerID.  The {cid} path should be replaced by \"*CustomerID*\".  The {amount} path should be replaced by \"*Amount*\".    ### Example URL  https://www.sportivity.com/sportivity-api/SportsrCredits/1234567/10      When adding credits the invoice will automatically be set to 'paid by IDeal'.      ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"CustomerId\": 1234567,    \"SportCredits\": 20.25,    \"FutureCredits\": 0,    \"VisitsPeriod\": 0,    \"Container_01\": 10.25,    \"Container_02\": 0,    \"Container_03\": 0,    \"Container_04\": 0,    \"Container_05\": 0,    \"Container_06\": 0,    \"Container_07\": 10,    \"Container_08\": 0,    \"Container_09\": 0,    \"Container_10\": 0,    \"Container_11\": 0,    \"Container_12\": 0  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No customer could be found based on the given ID|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\SportsCreditsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cid = 56; // int | 
$amount = 1.2; // float | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->sportsCreditsCidAmountPost($cid, $amount, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SportsCreditsApi->sportsCreditsCidAmountPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**|  |
 **amount** | **float**|  |
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sportsCreditsCidGet**
> string sportsCreditsCidGet($cid, $xAPITOKEN)

Retrieve sports credits of a customer by the customer ID

Retrieve sports credits of a customer by the customer ID.  The {cid} parameter should be replaced with the customer ID.    ### Example URL  https://www.sportivity.com/sportivity-api/SportsCredits/1234567    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {     \"CustomerId\": 3624696,     \"SportCredits\": 138,     \"FutureCredits\": 0,     \"VisitsPeriod\": 0,     \"Container_01\": 0,     \"Container_02\": 0,     \"Container_03\": 0,     \"Container_04\": 0,     \"Container_05\": 0,     \"Container_06\": 0,     \"Container_07\": 0,     \"Container_08\": 138,     \"Container_09\": 0,     \"Container_10\": 0,     \"Container_11\": 0,     \"Container_12\": 0,     \"Token\": \"eQCbIs6ivSLr3okD8Y7Oiki4eZImC0uAbwotNgF1O5giDDgwKpUyceDh5EZYOO+N/4yiaGcLqW8K4JeBPLeGUFbsRDwpq47wix4uSeO8cqPgoCx+ReTkZ6l14cBBw+0jwdKLknQaM4JbyePITXkMr0muWbgZYVTR/nztymoCfNs=\",     \"APIName\": \"SportsCredits\"  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404023 | No customer sportcredits could be found based on the given ID|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\SportsCreditsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cid = "cid_example"; // string | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->sportsCreditsCidGet($cid, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SportsCreditsApi->sportsCreditsCidGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **string**|  |
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

