# Funxtion\Integration\Sportivity\Api\Client\BlockageDefinitionApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blockageDefinitionGet**](BlockageDefinitionApi.md#blockagedefinitionget) | **GET** /BlockageDefinition | Retrieve All BlockageDefinitions from an location.

# **blockageDefinitionGet**
> string blockageDefinitionGet($xAPITOKEN)

Retrieve All BlockageDefinitions from an location.

Retrieve All BlockageDefinitions from an location that are active.  The BlockageDefinitionID is necessary to make a Post Suspension call.    ### Example URL  https://www.sportivity.com/sportivity-api/BlockageDefinition    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Response processing  You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an “Error” object with attributes “code” and \"message\".    ## Successful response sample  ```  {    \"Token\": \"eQCbIs6ivSLr3okD8Y7Oiki4eZImC0uAbwotNgF1O5giDDgwKpUyceDh5EZYOO+N/4yiaGcLqW8K4JeBPLeGUFbsRDwpq47wix4uSeO8cqPgoCx+ReTkZ6l14cBBw+0jwdKLknQaM4JbyePITXkMr0muWbgZYVTR/nztymoCfNs=\",    \"APIName\": \"GetBlockageDefinition\",    \"BlockageDefinitions\": [      {        \"BlockageDefinitionID\": 1002,        \"Description\": \"Zomerstop\",        \"BlockageType\": \"Bevriezing uitvoeren op einddatum contract\",        \"ExtraCosts\": 5,        \"ExtraCostsPeriod\": \"Per_maand\"      },      {        \"BlockageDefinitionID\": 1102,        \"Description\": \"Bevriezing - Direct + contract opschuiven\",        \"BlockageType\": \"Direct + opschuiven contractdatum\",        \"ExtraCosts\": 0      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404017 | No active blockagedefinitions found|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\BlockageDefinitionApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->blockageDefinitionGet($xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BlockageDefinitionApi->blockageDefinitionGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

