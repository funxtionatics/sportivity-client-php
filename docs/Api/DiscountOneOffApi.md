# Funxtion\Integration\Sportivity\Api\Client\DiscountOneOffApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**discountOneOffPost**](DiscountOneOffApi.md#discountoneoffpost) | **POST** /DiscountOneOff | Give a one time discount

# **discountOneOffPost**
> string discountOneOffPost($body, $xAPITOKEN)

Give a one time discount

Give a one time discount to a specific membership.    To give the discount as a percentage the body should contain the String Percentage as \"True\" or \"true\".    ### Example URL  https://www.sportivity.com/sportivity-api/DiscountOneOff    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ##Resource  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | MembershipId | Integer | Unique identifier of the membership.l |  | CustomerId | Integer | Unique identifier of the customer |  | ExecutionDate | Date |  Required. Important: the date format needs to be specified as dd/MM/yyyy, for example: 31/12/1999 |  | Amount | Decimal |  Required. Important: When Percentage is true this will become a percentage |  | Percentage | String | Standard false. Value should be \"True\" or \"true\" |      ## Sample message:  ```  {    \"CustomerId\": 1234567,    \"MembershipId\": 12345,    \"Description\": \"One month 50% off\",    \"Amount\": 50,    \"Percentage\": \"True\",    \"ExecutionDate\": \"01/02/2021\"  }  ```     ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | | No executiondate |  | 401 | 401001 | Unauthorized |  | 404 | | No membership found|  | 500 | | Could not format date|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\DiscountOneOffApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\DiscountResource(); // \Funxtion\Integration\Sportivity\Api\Client\Model\DiscountResource | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->discountOneOffPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountOneOffApi->discountOneOffPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\DiscountResource**](../Model/DiscountResource.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

