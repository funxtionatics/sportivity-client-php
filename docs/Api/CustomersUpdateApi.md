# Funxtion\Integration\Sportivity\Api\Client\CustomersUpdateApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customersUpdateGet**](CustomersUpdateApi.md#customersupdateget) | **GET** /CustomersUpdate | Retrieve all customer updates
[**customersUpdatePost**](CustomersUpdateApi.md#customersupdatepost) | **POST** /CustomersUpdate | Update a customer

# **customersUpdateGet**
> string customersUpdateGet($xAPITOKEN)

Retrieve all customer updates

Retrieve all customer updates.    This is a webhook. Making this call will result in a response where there are no updates.  To make use of this feature an endpoint must be given to the developers.    ### Example URL  https://www.sportivity.com/sportivity-api/CustomersUpdate    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"GetCustomersUpdate\": [      {        \"CustomerId\": 1234567,        \"Addition\": \"b\",        \"Address\": \"Frankweg\",        \"City\": \"Nieuw-Vennep\",        \"Email\": \"info1@boss.nl\",        \"FirstName\": \"Test1\",        \"HouseNumber\": \"41\",        \"LastName\": \"Tom1\",        \"Phone\": \"0252123456\",        \"PhoneMobile\": \"0612345679\",        \"ZipCode\": \"2153 PD\"      }    ]  }  ```    ## Successful empty response sample  ```  {\"GetCustomersUpdate\": [\"NoUpdates\"]}  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 |  | No updates have been found|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\CustomersUpdateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->customersUpdateGet($xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersUpdateApi->customersUpdateGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customersUpdatePost**
> string customersUpdatePost($body, $xAPITOKEN)

Update a customer

Supply a Customer resource in the body of the HTTP POST request.    ### Example URL  https://www.sportivity.com/sportivity-api/Customers    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Customer Resource  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | CustomerId | Integer | Unique identifier of the customer. Required |  | PassNumber | String(200) | Access card number. Optional |  | FirstName | String(200) | First name of the customer. Optional |  | Initials | String(40) | Initials. Optional |  | MiddleName | String(200) | Optional |  | LastName | String(200) |  Optional |  | Email | String(200) |  Optional |  | Country | Enumeration |  Optional. See below for enumeration options |  | PhoneMobile | String(200) |  Optional |  | PhonePrivate | String(200) |  Optional |  | Gender | Enumeration |  Optional. See below for enumeration options |  | Zipcode | String(200) |  Optional |  | City | String(200) |  Optional |  | Address | String(200) |  Optional |  | HouseNumber | String(200) |  Optional |  | HouseNumberExtension | String(50) |  Optional |  | BirthDate | Date |  Optional. Important: the date format needs to be specified as dd/MM/yyyy, for example: 31/12/1999 |  | IBAN | String(200) |  Optional. Bank account number |  | BIC | String(200) |  Optional. Note: If IBAN attribute is supplied, the BIC attribute is required |  | Language| Enumeration |   Optional. See below for enumeration options |  | OptIn| String |   Optional. For true: \"True\",\"true\", \"T\", \"t\". For false: \"False\",\"false\", \"F\", \"f\" |      ### Enumeration Country  - Nederland  - South_Africa  - Belgie  - Deutschland  - France  - Swiss  - Singapore  - Hongkong  - Austria  - Sweden  - USA  - Japan  - Kenia  - Noorwegen    ### Enumeration Gender  - Male  - Female    ### Enumeration Language  Currently the only 2 supported languages are Dutch and English:  - nl_NL  - en_US        ## Sample message:  ```  {   \"CustomerId\": \"\",   \"Initials\": \"\",   \"PassNumber\": \"\",   \"FirstName\": \"John\",   \"MiddleName\": \"\",   \"LastName\": \"Doe\",   \"Email\": \"johndoe@foo.bar\",   \"Country\": \"South_Africa\",   \"PhoneMobile\": \"\",   \"PhonePrivate\": \"\",   \"Gender\": \"\",   \"Zipcode\" : \"1234ID\",   \"City\": \"Atlantis\",   \"Address\" : \"Poseidon Street\",   \"HouseNumber\" : \"11\",   \"HouseNumberExtension\" : \"\",   \"BirthDate\": \"31/12/1999\",   \"IBAN\": \"NL16ABNA0839495696\",   \"BIC\": \"33\",   \"Language\": \"nl_NL\",   \"OptIn\": \"True\"  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400 | error parsing data|  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No customer could be found based on the given ID|  | 500 | 500 | Unexpected error|  | 500 | 500001 | error bankdetails|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\CustomersUpdateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\CustomerResource(); // \Funxtion\Integration\Sportivity\Api\Client\Model\CustomerResource | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->customersUpdatePost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersUpdateApi->customersUpdatePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\CustomerResource**](../Model/CustomerResource.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

