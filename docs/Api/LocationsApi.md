# Funxtion\Integration\Sportivity\Api\Client\LocationsApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**locationsGet**](LocationsApi.md#locationsget) | **GET** /Locations | Get the locations of a company

# **locationsGet**
> string locationsGet($xAPITOKEN)

Get the locations of a company

Retrieve all locations of a company    ### Example URL  https://www.sportivity.com/sportivity-api/Locations    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Sample response  ```  {    \"CompanyName\": \"b.o.s.s. BV\",    \"LocationResources\": [      {        \"LocationId\": 12345,        \"LocationName\": \"Location\",        \"Longitude\": 5.0866596,        \"Latitude\": 52.0672213,        \"Address\": \"Poseidon street\",        \"Phone\": \"0123-456789\",        \"HouseNumber\": \"11\",        \"ZipCode\": \"1234AB\",        \"City\": \"Atlantis\",        \"Country\": \"Curacao\"      }    ]  }    ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\LocationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->locationsGet($xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LocationsApi->locationsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

