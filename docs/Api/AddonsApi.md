# Funxtion\Integration\Sportivity\Api\Client\AddonsApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addonsGet**](AddonsApi.md#addonsget) | **GET** /Addons | Read addons
[**addonsMembershipdefinitionGet**](AddonsApi.md#addonsmembershipdefinitionget) | **GET** /Addons/Membershipdefinition | Retrieve addon prices by MembershipDefinitionId
[**addonsPost**](AddonsApi.md#addonspost) | **POST** /Addons | Activate and deactivate addons

# **addonsGet**
> string addonsGet($mid, $xAPITOKEN)

Read addons

Retrieve list of addons of a membership by the membership ID.  The {mid} query string should be replaced by \"*MembershipID*\".    The MembershipID can be found with the GetCustomer call setting the Mem value to \"True\"    ### Example URL  https://www.sportivity.com/sportivity-api/Addons?mid=11234  ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"MembershipId\": 123456,    \"CustomerId\": 1234567,    \"AddOns\": [      {        \"Active\": true,        \"Description\": \"Groupfitness\",        \"AddonOn\": true,        \"AddonID\": 654321,        \"FirstAmount\": 0,        \"NormalPriceDecimal\": 0,        \"Period\": \"Maand/maanden\"      },      {        \"Active\": false,        \"Description\": \"Zwembad\",        \"AddonOn\": true,        \"AddonID\": 765432,        \"FirstAmount\": 0,        \"NormalPriceDecimal\": 5,        \"Period\": \"Maand/maanden\"      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400018 | No mid |  | 401 | 401001 | Unauthorized |  | 404 | 404018 | No Membership found |

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\AddonsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mid = "mid_example"; // string | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->addonsGet($mid, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddonsApi->addonsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mid** | **string**|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addonsMembershipdefinitionGet**
> string addonsMembershipdefinitionGet($mdid, $startDate, $xAPITOKEN)

Retrieve addon prices by MembershipDefinitionId

Retrieve addon prices by MembershipDefinitionId.  The {mdid} path should be replaced by \"*MembershipDefinitionId*\". The {StartDate} query string can be replaced by \"StartDate=*Startdate*\", but can be left empty. When left empty the API uses the date of today.    To find the MembershipDefinitionId please refer to the MembershipDefinition tab.    If the start date is earlier than the payment date or the start date is in the future the addon prices will return 0.  The {StartDate} format should be dd/mm/yyyy.    ### Example URL  https://www.sportivity.com/sportivity-api/Addons/Membershipdefinition?mdid=12345&StartDate=01%2F01%2F2000    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"MembershipId\": 1234,    \"AddOns\": [      {        \"Active\": true,        \"Description\": \"Addon Tennis\",        \"AddonOn\": false,        \"FirstAmount\": 2,        \"NormalPriceDecimal\": 5,        \"Period\": \"Maand/maanden\",        \"PeriodNumber\": 1,        \"StartDate\": \"2019-12-23T00:00:00.000Z\"      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400014 | Error parsing startdate |  | 400 | 400020 | No mdid |  | 401 | 401001 | Unauthorized |  | 404 | 404002 | No Membership found|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\AddonsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mdid = "mdid_example"; // string | 
$startDate = "startDate_example"; // string | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->addonsMembershipdefinitionGet($mdid, $startDate, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddonsApi->addonsMembershipdefinitionGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mdid** | **string**|  | [optional]
 **startDate** | **string**|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addonsPost**
> string addonsPost($aid, $xAPITOKEN)

Activate and deactivate addons

Activate or deactivate an addon by its addon ID.  The {aid} path should be replaced by \"*AddonID*\".  If the addon is not active when making this call it will be activated. By calling when the addon is active it will be deactivated.    To find the AddonID please first use the Get Addons/{mid} call.    ### Example URL  https://www.sportivity.com/sportivity-api/Addons?aid=12345    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"MembershipId\": 123456,    \"CustomerId\": 1234567,    \"AddOns\": [      {        \"Active\": true,        \"Description\": \"Groupfitness\",        \"AddonOn\": false,        \"AddonID\": 654321,        \"NormalPriceDecimal\": 5,        \"Period\": \"Maand/maanden\",        \"Text\": \"Addon: Groupfitness is aangezet!\"      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400019 | No aid |  | 401 | 401001 | Unauthorized |  | 404 | 404018 | No Addon found |

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\AddonsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$aid = "aid_example"; // string | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->addonsPost($aid, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddonsApi->addonsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aid** | **string**|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

