# Funxtion\Integration\Sportivity\Api\Client\MembershipsCancelApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**membershipsCancelPost**](MembershipsCancelApi.md#membershipscancelpost) | **POST** /MembershipsCancel | Cancel a membership
[**membershipsCancelReasonsGet**](MembershipsCancelApi.md#membershipscancelreasonsget) | **GET** /MembershipsCancel/Reasons | Get the termination reasons for a membership

# **membershipsCancelPost**
> string membershipsCancelPost($body, $xAPITOKEN)

Cancel a membership

Supply a cancel resource in the body of the HTTP POST request.    ### Example URL  https://www.sportivity.com/sportivity-api/MembershipsCancel    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"      ## Sample message:  ```  {    \"CustomerId\": 1234567,    \"MembershipId\": 1234567,    \"CancelPerDate\": \"01/08/2020\",    \"TerminationId\": 1  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\".       ## Successful response sample:  ```  {    \"CustomerId\": 1234567,    \"CustomerName\": \"SwaggerupdateTest Doe\",    \"TerminationDescription\": \"Omzetting\",    \"CancelPerDate\": \"02-08-2020\"  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400014 | Could not parse date|  | 400 | 400024 | CancelledPerdate must be bigger than StartDate|  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No Customer could be found based on the given ID|  | 404 | 404021 | Membership not found|  | 404 | 404022 | Termination reason not found|  | 409 | 409009 | CancelledPerdate must be bigger than Paymentenddate|  | 409 | 409009 | Membership already cancelled|  | 500 | 500 | Unexpected error|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\MembershipsCancelApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\MembershipCancelResource(); // \Funxtion\Integration\Sportivity\Api\Client\Model\MembershipCancelResource | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->membershipsCancelPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MembershipsCancelApi->membershipsCancelPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\MembershipCancelResource**](../Model/MembershipCancelResource.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **membershipsCancelReasonsGet**
> string membershipsCancelReasonsGet($xAPITOKEN)

Get the termination reasons for a membership

Retrieve the termination reasons.      ### Example URL  https://www.sportivity.com/sportivity-api/MembershipsCancel/Reasons    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"Token\": \"fbvuwuneaskjfbjvbasidvugehb23879782y794tyv9n12y308u0m02=9uc89uyx08y10\",    \"APIName\": \"GetTerminateReason\",    \"TerminateReasons\": [      {        \"MembershipTerminationId\": 1,        \"Description\": \"Reason\",        \"CompanyLocation\": \"Location\"      },      {        \"MembershipTerminationId\": 2,        \"Description\": \"Reason\",        \"CompanyLocation\": \"Location\"      },      {        \"MembershipTerminationId\": 3,        \"Description\": \"Reason\",        \"CompanyLocation\": \"Location\"      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404022 | Termination reason not found|  | 500 | 500 | Unexpected error|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\MembershipsCancelApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->membershipsCancelReasonsGet($xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MembershipsCancelApi->membershipsCancelReasonsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

