# Funxtion\Integration\Sportivity\Api\Client\AdditionalInfoApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**additionalInfoDefinitionsGet**](AdditionalInfoApi.md#additionalinfodefinitionsget) | **GET** /AdditionalInfo/Definitions | Retrieve additional info defintions which can be used to set it to a customer.
[**additionalInfoFromCustomerCidGet**](AdditionalInfoApi.md#additionalinfofromcustomercidget) | **GET** /AdditionalInfo/FromCustomer/{cid} | Retrieve additional customer information set by the administrator of the company.
[**additionalInfoPost**](AdditionalInfoApi.md#additionalinfopost) | **POST** /AdditionalInfo | Post a new additional info to a customer

# **additionalInfoDefinitionsGet**
> string additionalInfoDefinitionsGet($xAPITOKEN)

Retrieve additional info defintions which can be used to set it to a customer.

### Example URL  https://www.sportivity.com/sportivity-api/AdditionalInfo/Definitions    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"      ## Sample response  ```  {    \"AdditionalInfoDatas\": [      {        \"AdditionalInfoType\": \"Date\",        \"Description\": \"Bevallingsdatum / Birth date\",        \"AddtitionalInfoID\": 1      },      {        \"AdditionalInfoType\": \"Date\",        \"Description\": \"Uitgerekende datum / Due date\",        \"AddtitionalInfoID\": 2      },      {        \"AdditionalInfoType\": \"String\",        \"Description\": \"Opmerking / Remark\",        \"AddtitionalInfoID\": 3      },      {        \"AdditionalInfoType\": \"String\",        \"Description\": \"RetentieCoach\",        \"AddtitionalInfoID\": 4      },      {        \"AdditionalInfoType\": \"String\",        \"Description\": \"Huisarts\",        \"AddtitionalInfoID\": 5      },      {        \"AdditionalInfoType\": \"String\",        \"Description\": \"Afspraken/uitzonderingen\",        \"AddtitionalInfoID\": 6      },      {        \"AdditionalInfoType\": \"String\",        \"Description\": \"Medicijnen\",        \"AddtitionalInfoID\": 7      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404009 | 'There are no additional information definitions available'|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\AdditionalInfoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->additionalInfoDefinitionsGet($xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdditionalInfoApi->additionalInfoDefinitionsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **additionalInfoFromCustomerCidGet**
> string additionalInfoFromCustomerCidGet($cid, $xAPITOKEN)

Retrieve additional customer information set by the administrator of the company.

The {cid} parameter should be replaced with the customer ID    ### Example URL  https://www.sportivity.com/sportivity-api/AdditionalInfo/FromCustomer/1234567    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"      ## Sample response  ```    {    \"CustomerId\": 1234567,    \"AdditionalInfoDatas\": [      {        \"AdditionalInfoType\": \"Date\",        \"Description\": \"Bevallingsdatum / Birth date\",        \"Value\": \"12-06-2020\"      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No customer found|  | 404 | 404008 | Customer has no additional info|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\AdditionalInfoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cid = 56; // int | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->additionalInfoFromCustomerCidGet($cid, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdditionalInfoApi->additionalInfoFromCustomerCidGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**|  |
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **additionalInfoPost**
> string additionalInfoPost($body, $xAPITOKEN)

Post a new additional info to a customer

### Example URL  https://www.sportivity.com/sportivity-api/AdditionalInfo    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## AdditionalInfo Resource  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | CustomerId | Integer | Unique identifier of the customer. Required |  | AdditionalinfoID | Integer | The definitionID of the Additional Info. Required |  | Value | String(200) | The value of the additional info. Required |      Important note:  Value is always a string in the json but depending on the type of the additional info it only accepts either String, Integer, Decimal or Date.  Date should be in format dd-mm-yyyy.    ## Sample message  ```  {    \"AdditionalInfoID\": 2,    \"CustomerID\": 1234567,    \"Value\": \"22-03-2020\"  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404009 | 'There are no additional information definitions available'|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\AdditionalInfoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\ImportAdditionalInfo(); // \Funxtion\Integration\Sportivity\Api\Client\Model\ImportAdditionalInfo | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->additionalInfoPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdditionalInfoApi->additionalInfoPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\ImportAdditionalInfo**](../Model/ImportAdditionalInfo.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

