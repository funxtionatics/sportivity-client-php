# Funxtion\Integration\Sportivity\Api\Client\CustomerMandateApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerMandateGet**](CustomerMandateApi.md#customermandateget) | **GET** /CustomerMandate | Retrieves the HarlandsID and mandatereference of a customer
[**customerMandatePost**](CustomerMandateApi.md#customermandatepost) | **POST** /CustomerMandate | Change the customers HarlandsId or MandateReference

# **customerMandateGet**
> string customerMandateGet($xAPITOKEN, $cid)

Retrieves the HarlandsID and mandatereference of a customer

The {cid} parameter should be replaced with the customer ID.    ### Example URL  https://www.sportivity.com/sportivity-api/CustomerMandate/1234567    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {     \"CustomerId\": 3624696,     \"HarlandsID\": 138,     \"MandateReference\":AVBaa33423q41,    }  ```

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\CustomerMandateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$cid = 56; // int | 

try {
    $result = $apiInstance->customerMandateGet($xAPITOKEN, $cid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerMandateApi->customerMandateGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]
 **cid** | **int**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerMandatePost**
> string customerMandatePost($xAPITOKEN)

Change the customers HarlandsId or MandateReference

### Example URL  https://www.sportivity.com/sportivity-api/CustomerMandate    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ##Resource  | Attribute name | Attribute Type | Description |   | ---------------------- | -----------------------| ------------------- |  | CustomerID | Integer | Unique identifier of the customer.l |  | MandateReference | String | MandateReference to change to, leave empty if no change |  | HarlandsID | Integer | HarlandsID to change to, leave empty if no change. |      ## Sample message:  ```  {   \"CustomerID\": 123456,   \"MandateReference\": \"AB1234\",   \"HarlandsID\": 5245  }  ```

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\CustomerMandateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->customerMandatePost($xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerMandateApi->customerMandatePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

