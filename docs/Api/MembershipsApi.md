# Funxtion\Integration\Sportivity\Api\Client\MembershipsApi

All URIs are relative to *https://www.sportivity.com/sportivity-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**membershipsMidGet**](MembershipsApi.md#membershipsmidget) | **GET** /Memberships/{mid} | Retrieve Membership by ID
[**membershipsNewMembershipsFromDateMembershipDescriptionGet**](MembershipsApi.md#membershipsnewmembershipsfromdatemembershipdescriptionget) | **GET** /Memberships/NewMemberships/{FromDate}/{MembershipDescription} | Get all new memberships from a certain date and an description
[**membershipsPost**](MembershipsApi.md#membershipspost) | **POST** /Memberships | Add a membership to a customer

# **membershipsMidGet**
> string membershipsMidGet($mid, $xAPITOKEN)

Retrieve Membership by ID

Retrieve a membership by the membership ID.  The {mid} parameter should be replaced with the membership ID,     The {mid} value can be found by setting the **Mem** value to true in the **GetCustomer** call.    ### Example URL  https://www.sportivity.com/sportivity-api/Memberships/1234567    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"CustomerName\": \"SwaggerupdateTest Doe\",    \"MembershipCreateDate\": \"15/12/2021\",    \"CompanyLocationName\": \"ACCP:Nieuw Vennep\",    \"CustomerId\": 12345678,    \"MembershipDescription\": \"Membership\",    \"MembershipId\": 1234567,    \"MembershipActive\": \"true\",    \"ContractDuration\": \"1\",    \"ContractDurationPeriod\": \"Maand/maanden\",    \"PaymentDuration\": \"1\",    \"PaymentDurationPeriod\": \"Maand/maanden\",    \"Visits\": \"0\",    \"AccesDuration\": \"1\",    \"AccesDurationPeriod\": \"Maand/maanden\",    \"Family\": true,    \"ContractEndDate\": \"14/01/2022\",    \"PaymentEndDate\": \"14/01/2022\",    \"CompanylocationID\": \"13000\",    \"CompanyMembershipdefinitionID\": \"4321\",    \"MembershipDefinitionID\": 1234,    \"LId_CMDId\": \"13000-4321\",    \"LastVisit\": \"17/12/2021\",    \"UseDiscount\": \"True\",    \"DiscountAsPercentage\": \"True\",    \"DiscountAmount\": 50,    \"Amount\": 100,    \"AmountWithDiscount\": 50,    \"UnlimitedVisits\": true,    \"Action\": true,    \"ActionDescription\":\"Eerste maand gratis\",    \"error\": {      \"HttpStatusCode\": 200    }  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 401 | 401001 | Unauthorized |  | 404 | 404021 | No membership could be found based on the given ID|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\MembershipsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mid = "mid_example"; // string | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->membershipsMidGet($mid, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MembershipsApi->membershipsMidGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mid** | **string**|  |
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **membershipsNewMembershipsFromDateMembershipDescriptionGet**
> string membershipsNewMembershipsFromDateMembershipDescriptionGet($fromDate, $membershipDescription, $xAPITOKEN, $tillDate)

Get all new memberships from a certain date and an description

Retrieve a membership by the membership ID.  The {FromDate} parameter should be replaced with the date from which you want the new membership.  The {TillDate} query string should be replaced with the date till which you want the new membership.  The {MembershipDescription} parameter should be replaced with the description or a part of the description of the membership.     If the number of days between FromDate and TillDate is bigger than 7, the TillDate is set to FromDate + 7.  If TillDate is left empty you will receive only the new memberships from the FromDate.    ### Example URL  https://www.sportivity.com/sportivity-api/Memberships/NewMemberships/01%2F01%2F2021/test?TillDate=01%2F02%2F2021    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ## Successful response sample  ```  {    \"MembershipDatas\": [      {        \"CustomerId\": 1234567,        \"CustomerName\": \"Test API\",        \"MembershipId\": 123456,        \"MembershipDescription\": \"*3 maanden \",        \"MembershipCreateDate\": \"10/02/2021\",        \"MembershipActive\": true,        \"CompanyLocationName\": \"ACCP:Nieuw Vennep\"      }    ]  }  ```    ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400 | Description not filled|  | 400 | 400 | Could not parse TillDate|  | 400 | 400 | Could not parse FromDate|  | 401 | 401001 | Unauthorized |  | 404 | 404 | No new memberships|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\MembershipsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$fromDate = "fromDate_example"; // string | 
$membershipDescription = "membershipDescription_example"; // string | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 
$tillDate = "tillDate_example"; // string | 

try {
    $result = $apiInstance->membershipsNewMembershipsFromDateMembershipDescriptionGet($fromDate, $membershipDescription, $xAPITOKEN, $tillDate);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MembershipsApi->membershipsNewMembershipsFromDateMembershipDescriptionGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fromDate** | **string**|  |
 **membershipDescription** | **string**|  |
 **xAPITOKEN** | **string**|  | [optional]
 **tillDate** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **membershipsPost**
> string membershipsPost($body, $xAPITOKEN)

Add a membership to a customer

Add a membership to a customer by the customer ID and membership DefinitionID.    ### Example URL  https://www.sportivity.com/sportivity-api/Memberships    ### HTTP header  The token parameter should be provided in a custom HTTP header with key \"X-API-TOKEN\"    ### Example sample  ```  {    \"CustomerId\": \"3051852\",    \"MembershipDefinitionID\": 1234,    \"StartDate\": \"01/01/2020\",    \"SendEmail\": false,    \"Base64Signature\": \"jnvqojebviwjebibqi3fbwqhvuebwibgfieuaieubwgq\",    \"Action\": false    }  ```    ### Example successfull response  ```  {      \"CustomerName\": \"Job Boss\",      \"MembershipCreateDate\": \"20/12/2019\",      \"CompanyLocationName\": \"Nieuw Vennep\",      \"CustomerID\": \"1234567\",      \"MembershipDescription\": \"Fitness Onbeperkt\",      \"MembershipID\": \"1234567\",      \"MembershipActive\": \"true\",      \"ContractDuration\": \"1\",      \"ContractDurationPeriod\": \"Maand/maanden\",      \"PaymentDuration\": \"1\",      \"PaymentDurationPeriod\": \"Maand/maanden\",      \"Visits\": \"0\",      \"AccesDuration\": \"1\",      \"AccesDurationPeriod\": \"Maand/maanden\",      \"Family\": false,      \"ContractEndDate\": \"01/02/2020\",      \"PaymentEndDate\": \"01/02/2020\",      \"CompanylocationID\": \"13000\",      \"CompanyMembershipdefinitionID\": \"\",      \"Token\": \"hQ/IuOVRlMe9tI9o=\",      \"APIName\": \"AddMembership\",      \"Action\": true,      \"ActionDescription\":\"Eerste maand gratis\",      \"error\": {          \"HttpStatusCode\": 200      },      \"MembershipDefinitionID\": 1234  }  ```    ### Important notes:    - When adding a membership that is supposed to have a group discount, the member first has to be part of a group. If the membership is added before the member is in a group the first period will be at full price.  - When using action = true, the action associated with the membershipdefinition will be performed.     ## Response processing    You will receive a synchronous HTTP response, with a JSON string in the response body. If something went wrong, the message will contain an \"Error\" object with attributes \"code\" and \"message\". See below for a list of possible codes/messages:    |HTTP Code | error code | Description |  |---|---|---|  | 200 | | Succes |  | 400 | 400014 | Could not parse date|  | 400 | 400023 | No IBAN|  | 401 | 401001 | Unauthorized |  | 404 | 404001 | No Customer could be found based on the given ID|  | 404 | 404002 | Membership definition not found|  | 500 | 500 | Unexpected error|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Funxtion\Integration\Sportivity\Api\Client\Api\MembershipsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Funxtion\Integration\Sportivity\Api\Client\Model\MembershipResource(); // \Funxtion\Integration\Sportivity\Api\Client\Model\MembershipResource | 
$xAPITOKEN = "xAPITOKEN_example"; // string | 

try {
    $result = $apiInstance->membershipsPost($body, $xAPITOKEN);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MembershipsApi->membershipsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\MembershipResource**](../Model/MembershipResource.md)|  | [optional]
 **xAPITOKEN** | **string**|  | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

