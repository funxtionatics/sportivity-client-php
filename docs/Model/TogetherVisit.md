# TogetherVisit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customersID** | **int** |  | [optional] 
**fullnameGuest** | **string** |  | [optional] 
**emailGuest** | **string** |  | [optional] 
**mobilePhoneGuest** | **string** |  | [optional] 
**dateVisitGuest** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

