# MembershipResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **int** |  | [optional] 
**membershipDefinitionID** | **int** |  | [optional] 
**startDate** | **string** |  | [optional] 
**sendEmail** | **bool** |  | [optional] 
**base64Signature** | **string** |  | [optional] 
**action** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

