# BlockageResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**membershipID** | **int** |  | [optional] 
**gateBlockage** | **string** |  | [optional] 
**blockageDefinitionID** | **int** |  | [optional] 
**startDate** | **string** |  | [optional] 
**endDate** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

