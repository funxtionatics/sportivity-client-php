# MembershipCancelResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **int** |  | [optional] 
**membershipId** | **int** |  | [optional] 
**terminationId** | **int** |  | [optional] 
**cancelPerDate** | **string** |  | [optional] 
**all** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

