# InvoiceResource2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **int** |  | [optional] 
**invoicePartialPayment** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\InvoiceResource2InvoicePartialPayment**](InvoiceResource2InvoicePartialPayment.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

