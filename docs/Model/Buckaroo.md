# Buckaroo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **int** |  | [optional] 
**amount** | **float** |  | [optional] 
**bank** | **string** |  | [optional] 
**paymentMethod** | **string** |  | [optional] 
**description** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

