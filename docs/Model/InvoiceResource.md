# InvoiceResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **int** |  | [optional] 
**invoices** | [**\Funxtion\Integration\Sportivity\Api\Client\Model\InvoiceResourceInvoices[]**](InvoiceResourceInvoices.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

