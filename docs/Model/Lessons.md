# Lessons

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **int** |  | [optional] 
**lessonId** | **int** |  | [optional] 
**lessonCancelled** | **bool** |  | [optional] 
**noShow** | **bool** |  | [optional] 
**useWaitlist** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

