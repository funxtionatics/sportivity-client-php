# CustomerResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**passNumber** | **string** |  | [optional] 
**firstName** | **string** |  | [optional] 
**middleName** | **string** |  | [optional] 
**lastName** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**phoneMobile** | **string** |  | [optional] 
**phonePrivate** | **string** |  | [optional] 
**gender** | **string** |  | [optional] 
**address** | **string** |  | [optional] 
**houseNumber** | **string** |  | [optional] 
**zipcode** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**birthDate** | **string** |  | [optional] 
**directDebitStatus** | **string** |  | [optional] 
**iBAN** | **string** |  | [optional] 
**bIC** | **string** |  | [optional] 
**language** | **string** |  | [optional] 
**customerId** | **int** |  | [optional] 
**country** | **string** |  | [optional] 
**title** | **string** |  | [optional] 
**initials** | **string** |  | [optional] 
**houseNumberExtension** | **string** |  | [optional] 
**optIn** | **string** |  | [optional] 
**useValidation** | **bool** |  | [optional] 
**isValidated** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

