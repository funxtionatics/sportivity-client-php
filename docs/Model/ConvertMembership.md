# ConvertMembership

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**convertDate** | **string** |  | [optional] 
**oldMembershipId** | **int** |  | [optional] 
**newMembershipDefinitionId** | **int** |  | [optional] 
**sendMail** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

