# LeadsResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fullname** | **string** |  | [optional] 
**firstname** | **string** |  | [optional] 
**middlename** | **string** |  | [optional] 
**lastname** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**additionalInfo** | **string** |  | [optional] 
**campaignName** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

