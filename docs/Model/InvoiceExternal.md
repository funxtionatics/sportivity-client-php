# InvoiceExternal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **int** |  | [optional] 
**openAmount** | **float** |  | [optional] 
**totalAmount** | **float** |  | [optional] 
**articleDefinitionId** | **int** |  | [optional] 
**directDebit** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

