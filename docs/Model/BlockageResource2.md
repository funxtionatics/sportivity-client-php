# BlockageResource2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**membershipID** | **int** |  | [optional] 
**blockageDefinitionID** | **int** |  | [optional] 
**startDate** | **string** |  | [optional] 
**endDate** | **string** |  | [optional] 
**blockageId** | **int** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

