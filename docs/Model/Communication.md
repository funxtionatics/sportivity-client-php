# Communication

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**note** | **string** |  | [optional] 
**customerId** | **int** |  | [optional] 
**dateTimeCreated** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**emailSubject** | **string** |  | [optional] 
**pDF1** | **string** |  | [optional] 
**pDF2** | **string** |  | [optional] 
**pDF3** | **string** |  | [optional] 
**pDF1Description** | **string** |  | [optional] 
**pDF2Description** | **string** |  | [optional] 
**pDF3Description** | **string** |  | [optional] 
**notificationID** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

