# ImportAdditionalInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additionalInfoID** | **int** |  | [optional] 
**customerID** | **int** |  | [optional] 
**value** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

