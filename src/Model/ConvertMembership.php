<?php
/**
 * ConvertMembership
 *
 * PHP version 5
 *
 * @category Class
 * @package  Funxtion\Integration\Sportivity\Api\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * SportivityAPI
 *
 * # Welcome to the Sportivity API Documentation    ## Basic concepts    - Message format is always in JSON  - In order to use the API, you will need an API key (token). Please contact info@boss.nl to obtain valid credentials  - Your token has to be provided in every message
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.32
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Funxtion\Integration\Sportivity\Api\Client\Model;

use \ArrayAccess;
use \Funxtion\Integration\Sportivity\Api\Client\ObjectSerializer;

/**
 * ConvertMembership Class Doc Comment
 *
 * @category Class
 * @package  Funxtion\Integration\Sportivity\Api\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ConvertMembership implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'ConvertMembership';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'convertDate' => 'string',
'oldMembershipId' => 'int',
'newMembershipDefinitionId' => 'int',
'sendMail' => 'bool'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'convertDate' => null,
'oldMembershipId' => 'int32',
'newMembershipDefinitionId' => 'int32',
'sendMail' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'convertDate' => 'ConvertDate',
'oldMembershipId' => 'OldMembershipId',
'newMembershipDefinitionId' => 'NewMembershipDefinitionId',
'sendMail' => 'SendMail'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'convertDate' => 'setConvertDate',
'oldMembershipId' => 'setOldMembershipId',
'newMembershipDefinitionId' => 'setNewMembershipDefinitionId',
'sendMail' => 'setSendMail'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'convertDate' => 'getConvertDate',
'oldMembershipId' => 'getOldMembershipId',
'newMembershipDefinitionId' => 'getNewMembershipDefinitionId',
'sendMail' => 'getSendMail'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['convertDate'] = isset($data['convertDate']) ? $data['convertDate'] : null;
        $this->container['oldMembershipId'] = isset($data['oldMembershipId']) ? $data['oldMembershipId'] : null;
        $this->container['newMembershipDefinitionId'] = isset($data['newMembershipDefinitionId']) ? $data['newMembershipDefinitionId'] : null;
        $this->container['sendMail'] = isset($data['sendMail']) ? $data['sendMail'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets convertDate
     *
     * @return string
     */
    public function getConvertDate()
    {
        return $this->container['convertDate'];
    }

    /**
     * Sets convertDate
     *
     * @param string $convertDate convertDate
     *
     * @return $this
     */
    public function setConvertDate($convertDate)
    {
        $this->container['convertDate'] = $convertDate;

        return $this;
    }

    /**
     * Gets oldMembershipId
     *
     * @return int
     */
    public function getOldMembershipId()
    {
        return $this->container['oldMembershipId'];
    }

    /**
     * Sets oldMembershipId
     *
     * @param int $oldMembershipId oldMembershipId
     *
     * @return $this
     */
    public function setOldMembershipId($oldMembershipId)
    {
        $this->container['oldMembershipId'] = $oldMembershipId;

        return $this;
    }

    /**
     * Gets newMembershipDefinitionId
     *
     * @return int
     */
    public function getNewMembershipDefinitionId()
    {
        return $this->container['newMembershipDefinitionId'];
    }

    /**
     * Sets newMembershipDefinitionId
     *
     * @param int $newMembershipDefinitionId newMembershipDefinitionId
     *
     * @return $this
     */
    public function setNewMembershipDefinitionId($newMembershipDefinitionId)
    {
        $this->container['newMembershipDefinitionId'] = $newMembershipDefinitionId;

        return $this;
    }

    /**
     * Gets sendMail
     *
     * @return bool
     */
    public function getSendMail()
    {
        return $this->container['sendMail'];
    }

    /**
     * Sets sendMail
     *
     * @param bool $sendMail sendMail
     *
     * @return $this
     */
    public function setSendMail($sendMail)
    {
        $this->container['sendMail'] = $sendMail;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
